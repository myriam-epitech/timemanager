defmodule Timemanager.Repo.Migrations.CreateCharts do
  use Ecto.Migration

  def change do
    create table(:charts) do
      add :user_id, :integer
      add :positionX, :float
      add :positionY, :float
      add :type, :string
      add :settings, :map

      timestamps()
    end
  end
end
