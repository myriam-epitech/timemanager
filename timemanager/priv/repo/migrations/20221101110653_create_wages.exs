defmodule Timemanager.Repo.Migrations.CreateWages do
  use Ecto.Migration

  def change do
    create table(:wages) do
      add :value, :float
      add :month, :utc_datetime # YYYY-MM-01THH:SS:MM
      timestamps()
    end

    alter table(:wages) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
    end
  end
end
