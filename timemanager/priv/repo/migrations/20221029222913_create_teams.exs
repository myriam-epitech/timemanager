defmodule Timemanager.Repo.Migrations.CreateTeams do
  use Ecto.Migration

  def change do
    create table(:teams) do
      add :name, :string
      add :total_hours, :float, default: 0.0
      add :hours_remaining, :float, default: 0.0

      timestamps()
    end
  end
end
