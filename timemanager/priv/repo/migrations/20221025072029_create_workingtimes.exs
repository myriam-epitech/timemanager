defmodule Timemanager.Repo.Migrations.CreateWorkingtimes do
  use Ecto.Migration

  def change do
    create table(:workingtimes) do
      add :start, :naive_datetime, null: false
      add :end, :naive_datetime, null: false
      add :comments, :string
      add :day_hours, :float
      add :night_hours, :float, default: 0.0

      timestamps()
    end

    alter table(:workingtimes) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :wt_type_id, references(:wt_types, on_delete: :delete_all),  default: 1
    end
    # create index(:workingtimes, [:user])
  end
end
