defmodule Timemanager.Repo.Migrations.CreateWTTeam do
  use Ecto.Migration

  def change do
    create table(:wt_teams) do
      add :wt_id, :integer
      add :team_id, :integer
      add :hour_worked, :float
      add :user_id, :integer

      timestamps()
    end

    create unique_index(:wt_teams, [:team_id, :wt_id])
  end
end
