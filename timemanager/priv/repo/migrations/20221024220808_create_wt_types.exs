defmodule Timemanager.Repo.Migrations.CreateWtTypes do
  use Ecto.Migration

  def change do
    create table(:wt_types) do
      add :name, :string

      timestamps()
    end

    create unique_index(:wt_types, [:name])

  end
end
