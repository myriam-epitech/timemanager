# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Timemanager.Repo.insert!(%Timemanager.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Timemanager.Repo
# alias Timemanager.{WTTypeContext.WTType, RoleContext.Role, UserContext.User, ClockContext.Clock, WTContext.WT, TeamContext.Team, WageContext.Wage}
alias Timemanager.{Repo, WTTypeContext.WTType, RoleContext.Role}

type1 = %WTType{ name: "working" }
type2 = %WTType{ name: "sick_leave"}
type3 = %WTType{ name: "vacation"}
type4 = %WTType{ name: "other" }

Repo.insert!(type1)
Repo.insert!(type2)
Repo.insert!(type3)
Repo.insert!(type4)

role1 = %Role{ name: "employee"}
role2 = %Role{ name: "manager"}
role3 = %Role{ name: "general_manager"}
role4 = %Role{ name: "administrator"}

Timemanager.Repo.insert!(role1)
Timemanager.Repo.insert!(role2)
Timemanager.Repo.insert!(role3)
Timemanager.Repo.insert!(role4)

# Repo.insert!(%User{username: "melvin", email: "melvin@gmail.com", role_id: 1, hourly_wage: 9.0})
# Repo.insert!(%WT{
#     start: ~N[2022-10-03 08:00:00],
#     end: ~N[2022-10-03 16:00:00],
#     day_hours: 5.0,
#     night_hours: 3.0,
#     total_hours: 8.0,
#     user_id: 1,
#     wt_type_id: 1
# })
# Repo.insert!(%WT{
#     start: ~N[2022-10-01 08:00:00],
#     end: ~N[2022-10-01 16:00:00],
#     day_hours: 8.0,
#     night_hours: 0.0,
#     total_hours: 8.0,
#     user_id: 1,
#     wt_type_id: 1
# })
# Repo.insert!(%WT{
#     start: ~N[2022-10-03 08:00:00],
#     end: ~N[2022-10-03 16:00:00],
#     day_hours: 0.0,
#     night_hours: 8.0,
#     total_hours: 8.0,
#     user_id: 1,
#     wt_type_id: 1
# })
# Repo.insert!(%WT{
#     start: ~N[2021-10-03 08:00:00],
#     end: ~N[2021-10-03 16:00:00],
#     day_hours: 5.0,
#     night_hours: 3.0,
#     total_hours: 8.0,
#     user_id: 1,
#     wt_type_id: 1
# })
# Repo.insert!(%WT{
#     start: ~N[2022-11-03 08:00:00],
#     end: ~N[2022-11-03 16:00:00],
#     day_hours: 5.0,
#     night_hours: 3.0,
#     total_hours: 8.0,
#     user_id: 1,
#     wt_type_id: 1
# })
# Repo.insert!(%WT{
#     start: ~N[2021-10-25 08:00:00],
#     end: ~N[2021-10-25 16:00:00],
#     day_hours: 5.0,
#     night_hours: 3.0,
#     total_hours: 8.0,
#     user_id: 1,
#     wt_type_id: 1
# })
# Repo.insert!(%WT{
#     start: ~N[2021-05-25 08:00:00],
#     end: ~N[2021-05-25 16:00:00],
#     day_hours: 5.0,
#     night_hours: 0.0,
#     total_hours: 5.0,
#     user_id: 1,
#     wt_type_id: 1
# })
# Repo.insert!(%Wage{value: 1420.0, user_id: 1, month: ~N[2022-10-01 00:00:00]})
