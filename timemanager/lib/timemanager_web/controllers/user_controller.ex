defmodule TimemanagerWeb.UserController do
  use TimemanagerWeb, :controller

  alias Timemanager.UserContext
  alias Timemanager.UserContext.User

  action_fallback TimemanagerWeb.FallbackController

  def index(conn, %{"username" => username, "email" => email}) do
    with {:ok, %User{} = user} <- UserContext.get_by_info(username, email) do
      render(conn, "show.json", user: user)
    end
  end

  def index(conn, %{"username" => username}) do
    with {:ok, %User{} = user} <- UserContext.get_by_username(username) do
      render(conn, "show.json", user: user)
    end
  end

  def index(conn, %{"email" => email}) do
    with {:ok, %User{} = user} <- UserContext.get_by_email(email) do
      render(conn, "show.json", user: user)
    end
  end

  def index(conn, _params) do
    with {:ok, users} <- UserContext.list_users() do
      render(conn, "index.json", users: users)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, %User{} = user} <- UserContext.get_user(id) do
      render(conn, "show.json", user: user)
    end
  end

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- UserContext.create_user(user_params) do
      conn
      |> put_status(:created)
      |> render("show.json", user: user)
    end
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    with {:ok, %User{} = user_to_update} <- UserContext.get_user(id) do
      with {:ok, %User{} = user} <- UserContext.update_user(user_to_update, user_params) do
        render(conn, "show.json", user: user)
      end
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, %User{} = user_to_delete} <- UserContext.get_user(id) do
      with {:ok, _resp} <- UserContext.delete_user(user_to_delete) do
        send_resp(conn, :no_content, "")
      end
    end
  end
end
