defmodule TimemanagerWeb.ClockController do
  use TimemanagerWeb, :controller

  alias Timemanager.ClockContext
  alias Timemanager.ClockContext.Clock

  action_fallback TimemanagerWeb.FallbackController

  def getByUserID(conn, %{"userID" => userID}) do
    with {:ok, %Clock{} = clock} <- ClockContext.getByUserID(userID) do
      render(conn, "show.json", clock: clock)
    end
  end

  def create(conn, %{"clock" => clock_params, "userID" => userID}) do
    {user_id, ""} = Integer.parse(userID)

    with {:ok, %Clock{} = clock} <- ClockContext.create_clock(clock_params, user_id) do
      conn
      |> render("show.json", clock: clock)
    end
  end
end
