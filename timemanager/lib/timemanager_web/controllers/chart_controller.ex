defmodule TimemanagerWeb.ChartController do
  use TimemanagerWeb, :controller

  alias Timemanager.ChartContext
  alias Timemanager.ChartContext.Chart

  action_fallback TimemanagerWeb.FallbackController

  def index(conn, _params) do
    charts = ChartContext.list_charts()
    render(conn, "index.json", charts: charts)
  end

  def create(conn, %{"chart" => chart_params}) do
    with {:ok, %Chart{} = chart} <- ChartContext.create_chart(chart_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.chart_path(conn, :show, chart))
      |> render("show.json", chart: chart)
    end
  end

  def show(conn, %{"id" => id}) do
    chart = ChartContext.get_chart!(id)
    render(conn, "show.json", chart: chart)
  end

  def update(conn, %{"id" => id, "chart" => chart_params}) do
    chart = ChartContext.get_chart!(id)

    with {:ok, %Chart{} = chart} <- ChartContext.update_chart(chart, chart_params) do
      render(conn, "show.json", chart: chart)
    end
  end

  def delete(conn, %{"id" => id}) do
    chart = ChartContext.get_chart!(id)

    with {:ok, %Chart{}} <- ChartContext.delete_chart(chart) do
      send_resp(conn, :no_content, "")
    end
  end
end
