defmodule TimemanagerWeb.WTTeamController do
  use TimemanagerWeb, :controller

  alias Timemanager.WTTeamContext
  alias Timemanager.WTTeamContext.WTTeam

  action_fallback TimemanagerWeb.FallbackController

  def addWTProject(conn, %{"wt_team" => wt_team_params, "userID" => user_id, "idTeam" => team_id, "idWT" => wt_id}) do
    {team_id, ""} = Integer.parse(team_id)
    {wt_id, ""} = Integer.parse(wt_id)
    {user_id, ""} = Integer.parse(user_id)

    with {:ok, %WTTeam{} = wt_team} <-
           WTTeamContext.add_wt_project(wt_team_params, team_id, wt_id, user_id) do
      render(conn, "show.json", wt_team: wt_team)
    end
  end

  def index(conn, _params) do
    wt_teams = WTTeamContext.list_wt_teams()
    render(conn, "index.json", wt_teams: wt_teams)
  end

  def create(conn, %{"wt_team" => wt_team_params}) do
    with {:ok, %WTTeam{} = wt_team} <- WTTeamContext.create_wt_team(wt_team_params) do
      render(conn, "show.json", wt_team: wt_team)
    end
  end

  def show(conn, %{"id" => id}) do
    wt_team = WTTeamContext.get_wt_team!(id)
    render(conn, "show.json", wt_team: wt_team)
  end

  def update(conn, %{"id" => id, "wt_team" => wt_team_params}) do
    wt_team = WTTeamContext.get_wt_team!(id)

    with {:ok, %WTTeam{} = wt_team} <- WTTeamContext.update_wt_team(wt_team, wt_team_params) do
      render(conn, "show.json", wt_team: wt_team)
    end
  end

  def delete(conn, %{"id" => id}) do
    wt_team = WTTeamContext.get_wt_team!(id)

    with {:ok, %WTTeam{}} <- WTTeamContext.delete_wt_team(wt_team) do
      send_resp(conn, :no_content, "")
    end
  end
end
