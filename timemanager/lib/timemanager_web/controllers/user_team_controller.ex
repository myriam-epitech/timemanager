defmodule TimemanagerWeb.UserTeamController do
  use TimemanagerWeb, :controller

  alias Timemanager.UserTeamContext
  alias Timemanager.UserTeamContext.UserTeam

  action_fallback TimemanagerWeb.FallbackController

  @spec getUsersManaged(Plug.Conn.t(), any) :: Plug.Conn.t()
  def getUsersManaged(conn, %{"managerID" => managerID}) do
    with {:ok, users} <- UserTeamContext.get_users_managed(managerID) do
      conn
      |> put_view(TimemanagerWeb.UserView)
      |> render("index.json", users: users)
    end
  end

  def addManagerToTeam(conn, %{"is_manager" => is_manager, "userID" => userID, "id" => id}) do
    {team_id, ""} = Integer.parse(id)
    {user_id, ""} = Integer.parse(userID)
    user_team = UserTeamContext.add_manager_to_team(is_manager, user_id, team_id)
    # with {:ok, %UserTeam{} = user_team} <-
    #        UserTeamContext.add_manager_to_team(is_manager, user_id, team_id) do
      conn
      |> render("show.json", user_team: user_team)
    # end
  end

  def addUserToTeam(conn, %{"userID" => userID, "id" => id}) do
    {team_id, ""} = Integer.parse(id)
    {user_id, ""} = Integer.parse(userID)

    with {:ok, %UserTeam{} = user_team} <- UserTeamContext.add_user_to_team(user_id, team_id) do
      conn
      # user = UserTeamContext.add_user_to_team(user_id, team_id)
      |> render("show.json", user_team: user_team)
    end
  end

  def removeUserFromTeam(conn, %{"id" => id, "userID" => userID}) do
    with {:ok, %UserTeam{}} <- UserTeamContext.remove_user_from_team(id, userID) do
      send_resp(conn, :no_content, "")
    end
  end
end
