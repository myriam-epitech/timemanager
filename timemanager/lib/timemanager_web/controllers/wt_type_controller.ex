defmodule TimemanagerWeb.WTTypeController do
  use TimemanagerWeb, :controller

  alias Timemanager.WTTypeContext
  alias Timemanager.WTTypeContext.WTType

  action_fallback TimemanagerWeb.FallbackController

  def index(conn, _params) do
    wt_types = WTTypeContext.list_wt_types()
    render(conn, "index.json", wt_types: wt_types)
  end

  def create(conn, %{"wt_type" => wt_type_params}) do
    with {:ok, %WTType{} = wt_type} <- WTTypeContext.create_wt_type(wt_type_params) do
      render(conn, "show.json", wt_type: wt_type)
    end
  end

  def show(conn, %{"id" => id}) do
    wt_type = WTTypeContext.get_wt_type!(id)
    render(conn, "show.json", wt_type: wt_type)
  end

  def update(conn, %{"id" => id, "wt_type" => wt_type_params}) do
    wt_type = WTTypeContext.get_wt_type!(id)

    with {:ok, %WTType{} = wt_type} <- WTTypeContext.update_wt_type(wt_type, wt_type_params) do
      render(conn, "show.json", wt_type: wt_type)
    end
  end

  def delete(conn, %{"id" => id}) do
    wt_type = WTTypeContext.get_wt_type!(id)

    with {:ok, %WTType{}} <- WTTypeContext.delete_wt_type(wt_type) do
      send_resp(conn, :no_content, "")
    end
  end
end
