defmodule TimemanagerWeb.WageController do
  use Timex
  import WaitForIt
  use TimemanagerWeb, :controller

  alias Timemanager.WageContext
  alias Timemanager.WageContext.Wage
  alias Timemanager.WTContext

  action_fallback TimemanagerWeb.FallbackController

  def index(conn, _params) do
    wages = WageContext.list_wages()
    render(conn, "index.json", wages: wages)
  end

  def create(conn, %{"wage" => wage_params}) do
    with {:ok, %Wage{} = wage} <- WageContext.create_wage(wage_params) do
      render(conn, "show.json", wage: wage)
    end
  end

  def calcWage(user_id, month) do
    # calc the wage of a month
    # Use first days of this month and the next to delimit the current month
    next_month = Timex.shift(month, months: 1)

    # Get all WTs of the month
    {err, workingtimes} = WTContext.getAllByUserIDAndParams(user_id, month, next_month)

    cond do
      err == :error ->
        :noop

      err == :ok ->
        wage_value =
          cond do
            length(workingtimes) != 0 ->
              # Sum the appropriate hourly wages for all the day/night hours worked
              total_WT =
                Enum.reduce(workingtimes, fn wt, acc ->
                  %{
                    user: wt.user,
                    day_hours: wt.day_hours + acc.day_hours,
                    night_hours: wt.night_hours + acc.night_hours
                  }
                end)

              # night hours wage
              wage_value =
                total_WT.day_hours * total_WT.user.hourly_wage +
                  total_WT.night_hours * total_WT.user.hourly_wage * 1.5

              # Add the calculated wage to the DB
              # WaitForIt.wait {^:ok, data} == WageContext.create_wage(%{user_id: user_id, value: wage_value, month: month})
              WageContext.create_wage(%{user_id: user_id, value: wage_value, month: month})

            true ->
              :noop
          end

      true ->
        :noop
    end
  end

  def show(conn, %{"id" => id}) do
    wage = WageContext.get_wage!(id)
    render(conn, "show.json", wage: wage)
  end

  def getAll(conn, %{"userID" => user_id}) do
    # Check that each month's wage is in the db, calculate it if it is not, then render waages
    # Get all workingtimes of the user
    wages = WageContext.get_wage!(user_id)

    # Get first and last time the user has worked...

    with {:ok, first} <- WTContext.getFirst!(user_id) do
      with {:ok, last} <- WTContext.getLast!(user_id) do
        period = %{first_wt: first, last_wt: last}
        s = Timex.set(period.first_wt.start, day: 0, hour: 0, minute: 0, second: 0)
        e = Timex.set(period.last_wt.end, day: 0, hour: 0, minute: 0, second: 0)
        # ...and make sure every month in between is associated to a wage
        calcMissingWages(user_id, wages, %{first: s, last: e})

        # get wages again once missing wages have been calculated
        wages = WageContext.get_wage!(user_id)

        render(conn, "index.json", wages: wages)
      end
    end
  end

  def calcMissingWages(user_id, wages, %{:first => s, :last => e}) do
    # Calculate the wages missing from the wages table

    # recursively iterate over months, checking if the wage is in the table and calculating it when it's not
    cond do
      Timex.before?(s, e) ->
        calcIfMissing(user_id, s, wages)
        s = Timex.shift(s, months: 1)
        calcMissingWages(user_id, wages, %{first: s, last: e})

      true ->
        :noop
        # IO.puts("=== FINISHED ITERATING OVER MONTHS ====")
    end
  end

  def calcIfMissing(user_id, month, wages) do
    # Calc wage only if there is no row for this month in the wages table
    cond do
      !Enum.any?(wages, &(&1.month == month)) -> calcWage(user_id, month)
      true -> :noop
    end
  end

  def update(conn, %{"id" => id, "wage" => wage_params}) do
    wage = WageContext.get_wage!(id)

    with {:ok, %Wage{} = wage} <- WageContext.update_wage(wage, wage_params) do
      render(conn, "show.json", wage: wage)
    end
  end

  def delete(conn, %{"id" => id}) do
    wage = WageContext.get_wage!(id)

    with {:ok, %Wage{}} <- WageContext.delete_wage(wage) do
      send_resp(conn, :no_content, "")
    end
  end
end
