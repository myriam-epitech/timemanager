defmodule TimemanagerWeb.AuthController do
  use TimemanagerWeb, :controller

  alias Timemanager.UserContext
  alias Timemanager.UserContext.User

  alias TimemanagerWeb.JWTToken

  action_fallback TimeManagerWeb.FallbackController

  def login(conn, %{"email" => email, "password" => password}) do
    with {:ok, %User{} = user_by_email} <- UserContext.get_by_email(email) do
      with {:ok, %User{} = user} <-
             Bcrypt.check_pass(user_by_email, password, hash_key: :password_hash) do
        signer =
          Joken.Signer.create(
            "HS256",
            "v5ZUl4SnVIiV6a4O3TeEJnI4OwhW9cJc5vNPvOdIyEOty9t4pVYfDXLq6rPf1PXQ"
          )

        expire = DateTime.add(DateTime.utc_now(), 36000, :second)

        extra_claims = %{
          "user_id" => user.id,
          "role" => user.role.id,
          "expiry" => expire
        }

        {:ok, token, _claims} = JWTToken.generate_and_sign(extra_claims, signer)
        {:ok, _claims} = JWTToken.verify_and_validate(token, signer)

        conn
        |> render("token_message.json", %{status: 1, message: "Login Successful", token: token})
      else
        _ ->
          conn
          |> put_status(:unauthorized)
          |> render("token_error.json", %{error: "Invalid credentials"})
      end
    end
  end

  def get_current(conn, _params) do
    user_id = conn.assigns.user_id

    with {:ok, %User{} = user} <- UserContext.get_user(user_id) do
      render(conn, "user.json", %{data: user})
    end
  end

  def logout(conn, _params) do
    # put_req_header(conn, "Bearer", "")
    send_resp(conn, 201, "Logged out")
  end
end
