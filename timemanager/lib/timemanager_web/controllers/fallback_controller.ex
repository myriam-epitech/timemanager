defmodule TimemanagerWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use TimemanagerWeb, :controller

  # This clause handles errors returned by Ecto's insert/update/delete.
  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(TimemanagerWeb.ChangesetView)
    |> render("error.json", changeset: changeset)
  end

  # 404 422 403 501 400 This clause is an example of how to handle resources that cannot be found.
  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(TimemanagerWeb.ErrorView)
    |> render("error.json", %{status: 0, message: "Not found"})
  end

  def call(conn, {:error, :no_content}) do
    conn
    |> put_status(:no_content)
    |> put_view(TimemanagerWeb.ErrorView)
    |> render("error.json", %{status: 0, message: "No content found"})
  end

  def call(conn, {:error, :other_errors}) do
    conn
    |> put_status(:internal_server_error)
    |> put_view(TimemanagerWeb.ErrorView)
    |> render("error.json", %{status: 0, message: "Server error"})
  end
end
