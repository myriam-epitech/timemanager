defmodule TimemanagerWeb.WTController do
  use TimemanagerWeb, :controller

  alias Timemanager.WTContext
  alias Timemanager.WTContext.WT

  action_fallback TimemanagerWeb.FallbackController

  def getAll(conn, %{"userID" => user_id, "start" => param_start, "end" => param_end}) do
    with {:ok, workingtimes} <- WTContext.getAllByUserIDAndParams(user_id, param_start, param_end) do
      render(conn, "index.json", workingtimes: workingtimes)
    end
  end

  def getOne(conn, %{"userID" => user_id, "which" => param_which}) do
    workingtime = {}
    case param_which do
      "first" -> workingtime = WTContext.getFirst!(user_id)
      "last"  -> workingtime = WTContext.getLast!(user_id)
    end
    |> render(conn, "show.json", wt: workingtime)
  end

  def getAll(conn, %{"userID" => user_id}) do
    with {:ok, workingtimes} <- WTContext.getAllByUserID(user_id) do
      render(conn, "index.json", workingtimes: workingtimes)
    end
  end

  def getOne(conn, %{"userID" => user_id, "id" => id}) do
    with {:ok, %WT{} = workingtime} <- WTContext.getOneByUserID(user_id, id) do
      render(conn, "show.json", wt: workingtime)
    end
  end

  def create(conn, %{"wt" => wt_params, "userID" =>userID}) do
    {user_id, ""}  = Integer.parse(userID)
    with {:ok, %WT{} = wt} <- WTContext.create_wt(wt_params, user_id) do
      conn
      |> put_status(201)
      |> render("show.json", wt: wt)
    end
  end

  def update(conn, %{"id" => id, "wt" => wt_params}) do
    with {:ok, %WT{} = workingtime} <- WTContext.get_wt(id) do
      with {:ok, %WT{} = wt} <- WTContext.update_wt(workingtime, wt_params) do
        render(conn, "show.json", wt: wt)
      end
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, %WT{} = workingtime} <- WTContext.get_wt(id) do
      with {:ok, %WT{}} <- WTContext.delete_wt(workingtime) do
        send_resp(conn, :no_content, "")
      end
    end
  end
end
