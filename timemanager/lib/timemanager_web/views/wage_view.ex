defmodule TimemanagerWeb.WageView do
  use TimemanagerWeb, :view
  alias TimemanagerWeb.WageView

  def render("index.json", %{wages: wages}) do
    %{data: render_many(wages, WageView, "wage.json")}
  end

  def render("show.json", %{wage: wage}) do
    %{data: render_one(wage, WageView, "wage.json")}
  end

  def render("wage.json", %{wage: wage}) do
    %{
      id: wage.id,
      value: wage.value,
      user_id: wage.user_id,
      month: wage.month
    }
  end
end
