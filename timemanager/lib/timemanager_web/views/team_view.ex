defmodule TimemanagerWeb.TeamView do
  use TimemanagerWeb, :view
  alias TimemanagerWeb.TeamView


  def render("index.json", %{teams: teams}) do
    %{data: render_many(teams, TeamView, "team.json")}
  end

  def render("show.json", %{team: team}) do
    %{data: render_one(team, TeamView, "team.json")}
  end

  def render("team.json", %{team: team}) do
    %{
      id: team.id,
      name: team.name,
      total_hours: team.total_hours,
      hours_remaining: team.hours_remaining,
      users: render_many(team.users, TimemanagerWeb.UserView, "user.json"),
      workingtimes: render_many(team.workingtimes, TimemanagerWeb.WTView, "wt.json")
    }
  end


  # CUSTOM
  def render("custom_index.json", %{teams: teams}) do
    %{data: render_many(teams, TeamView, "custom_team.json")}
  end

  def render("custom_team.json", %{team: team}) do
    %{
      id: team.id,
      name: team.name,
      total_hours: team.total_hours,
      hours_remaining: team.hours_remaining,
      users: render_many(team.users, TimemanagerWeb.UserView, "custom_user.json"),
    }
  end

  # PROJECTS
  def render("projects_by_user.json", %{teams: teams}) do
    %{data: render_many(teams, TeamView, "project_by_user.json")}
  end

  def render("project_by_user.json", %{team: team}) do
    %{
      id: team.id,
      name: team.name,
      total_hours: team.total_hours,
      hours_remaining: team.hours_remaining,
      workingtimes: render_many(team.workingtimes, TimemanagerWeb.WTView, "wt_projects.json")
    }
  end

  # GET ALL
  def render("index_get_all.json", %{teams: teams}) do
    %{data: render_many(teams, TeamView, "get_one.json")}
  end

  def render("get_one.json", %{team: team}) do
    %{
      id: team.id,
      name: team.name,
      total_hours: team.total_hours,
      hours_remaining: team.hours_remaining,
      users: render_many(team.users, TimemanagerWeb.UserView, "user.json"),
    }
  end
end
