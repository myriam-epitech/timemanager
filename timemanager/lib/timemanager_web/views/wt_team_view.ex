defmodule TimemanagerWeb.WTTeamView do
  use TimemanagerWeb, :view
  alias TimemanagerWeb.WTTeamView

  def render("index.json", %{wt_teams: wt_teams}) do
    %{data: render_many(wt_teams, WTTeamView, "wt_team.json")}
  end

  def render("show.json", %{wt_team: wt_team}) do
    %{data: render_one(wt_team, WTTeamView, "wt_team.json")}
  end

  def render("wt_team.json", %{wt_team: wt_team}) do
    %{
      id: wt_team.id,
      wt_id: wt_team.wt_id,
      team_id: wt_team.team_id,
      hour_worked: wt_team.hour_worked
    }
  end
end
