defmodule TimemanagerWeb.UserView do
  use TimemanagerWeb, :view
  alias TimemanagerWeb.UserView
  alias TimemanagerWeb.RoleView


  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      username: user.username,
      email: user.email,
      role_id: user.role_id,
      role: RoleView.render("show.json", %{role: user.role}),
      hourly_wage: user.hourly_wage,
      night_hours_month: user.night_hours_month,
      # teams: render_many(user.teams, TimemanagerWeb.TeamView, "team.json")
      # workingtimes: render_many(user.workingtimes, TimemanagerWeb.WTView, "wt.json")
      # clocks: TimemanagerWeb.ClockView.render("show.json", %{clock: user.clocks}),

    }
  end

  def render("custom_user.json", %{user: user}) do
    %{
      id: user.id,
      username: user.username,
      email: user.email,
      role_id: user.role_id,
      role: RoleView.render("show.json", %{role: user.role}),
      hourly_wage: user.hourly_wage,
      night_hours_month: user.night_hours_month,
      hour_worked: user.hour_worked,
      is_manager: user.is_manager
    }
  end
end
