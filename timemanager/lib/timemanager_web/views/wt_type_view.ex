defmodule TimemanagerWeb.WTTypeView do
  use TimemanagerWeb, :view
  alias TimemanagerWeb.WTTypeView

  def render("index.json", %{wt_types: wt_types}) do
    %{data: render_many(wt_types, WTTypeView, "wt_type.json")}
  end

  def render("show.json", %{wt_type: wt_type}) do
    %{data: render_one(wt_type, WTTypeView, "wt_type.json")}
  end

  def render("wt_type.json", %{wt_type: wt_type}) do
    %{
      id: wt_type.id,
      name: wt_type.name
    }
  end
end
