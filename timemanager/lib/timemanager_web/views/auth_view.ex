defmodule TimemanagerWeb.AuthView do
  use TimemanagerWeb, :view
  # alias TimemanagerWeb.AuthView

  def render("user.json", %{data: user}) do
    %{
      status: 1,
      data: %{
        user: %{
          id: user.id,
          username: user.username,
          email: user.email,
          role_id: user.role_id,
          hourly_wage: user.hourly_wage,
          night_hours_month: user.night_hours_month
        }
      }
    }
  end

  #CUSTOM

  def render("token_message.json", %{status: status, message: message, token: token}) do
    %{
      status: status,
      message: message,
      token: token
    }
  end

    def render("token_errors.json", %{errors: errors}) do
      %{
        status: 0,
        errors: errors,
      }
    end

  def render("token_error.json", %{error: error}) do
    %{
      status: 0,
      error: error,
    }
  end
end
