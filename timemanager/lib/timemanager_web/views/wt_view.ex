defmodule TimemanagerWeb.WTView do
  use TimemanagerWeb, :view
  alias TimemanagerWeb.WTView

  def render("index.json", %{workingtimes: workingtimes}) do
    %{data: render_many(workingtimes, WTView, "wt.json")}
  end

  def render("show.json", %{wt: wt}) do
    %{data: render_one(wt, WTView, "wt.json")}
  end

  def render("wt.json", %{wt: wt}) do
    if wt.wt_type do
      %{
        id: wt.id,
        start: wt.start,
        end: wt.end,
        comments: wt.comments,
        day_hours: wt.day_hours,
        night_hours: wt.night_hours,
        user_id: wt.user_id,
        wt_type: TimemanagerWeb.WTTypeView.render("wt_type.json", %{wt_type: wt.wt_type}),
        user: TimemanagerWeb.UserView.render("user.json", %{user: wt.user})
      }
    else
      %{
        id: wt.id,
        start: wt.start,
        end: wt.end,
        comments: wt.comments,
        day_hours: wt.day_hours,
        night_hours: wt.night_hours,
        user_id: wt.user_id,
        user: TimemanagerWeb.UserView.render("user.json", %{user: wt.user})
      }
    end
  end

  # CUSTOM

  def render("wt_projects.json", %{wt: wt}) do
    %{
      id: wt.id,
      start: wt.start,
      end: wt.end,
      comments: wt.comments,
      day_hours: wt.day_hours,
      night_hours: wt.night_hours,
      user_id: wt.user_id,
      hour_worked: wt.hour_worked,
      wt_type: TimemanagerWeb.WTTypeView.render("wt_type.json", %{wt_type: wt.wt_type}),
    }
  end
end
