defmodule TimemanagerWeb.Router do
  use TimemanagerWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :auth do
    plug(:fetch_session)
    plug Timemanager.JWTAuthPlug
  end

  scope "/api", TimemanagerWeb do
    pipe_through :api
    # pipe_through :auth
    resources "/users", UserController, except: [:new, :edit]
    resources "/workingtimes", WTController, only: [:update, :delete]
    get "/workingtimes/:userID", WTController, :getAll
    get "/workingtimes/:userID", WTController, :getOne
    get "/workingtimes/:userID/:id", WTController, :getOne
    post "/workingtimes/:userID", WTController, :create

    get "/clocks/:userID", ClockController, :getByUserID
    post "/clocks/:userID", ClockController, :create

    post "/roles", RoleController, :create

    resources "/teams", TeamController, only: [:update, :delete, :create]
    get "/teams", TeamController, :index
    get "/teams/manager/:managerID", TeamController, :getByManager
    get "/projects/:userID", TeamController, :getProjectsByUser

    get "/users/manager/:managerID", UserTeamController, :getUsersManaged
    delete "/teams/:userID/:id", UserTeamController, :removeUserFromTeam
    post "/teams/:userID/:id", UserTeamController, :addUserToTeam
    put "/teams/:userID/:id", UserTeamController, :addManagerToTeam

    get "/wt/types", WTTypeController, :index

    resources "/charts", ChartController, except: [:new, :edit]

    post "/users/:userID/teams/:idTeam/wt/:idWT", WTTeamController, :addWTProject

    get "/wages/:userID", WageController, :getAll
  end

  scope "/api", TimemanagerWeb do
    pipe_through :api
    pipe_through :auth

    get "/currentUser", AuthController, :get_current
    get "/logout", AuthController, :logout
  end

  scope "/api", TimemanagerWeb do
    pipe_through :api

    post "/login", AuthController, :login
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).

  # if Mix.env() in [:dev, :test] do
  #   import Phoenix.LiveDashboard.Router

  #   scope "/" do
  #     pipe_through [:fetch_session, :protect_from_forgery]

  #     live_dashboard "/dashboard", metrics: TimemanagerWeb.Telemetry
  #   end
  # end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.

#   if Mix.env() == :dev do
#     scope "/dev" do
#       pipe_through [:fetch_session, :protect_from_forgery]

#       forward "/mailbox", Plug.Swoosh.MailboxPreview
#     end
#   end
end
