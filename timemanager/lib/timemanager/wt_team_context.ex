defmodule Timemanager.WTTeamContext do
  @moduledoc """
  The WTTeamContext context.
  """

  import Ecto.Query, warn: false
  alias Timemanager.Repo

  alias Timemanager.WTTeamContext.WTTeam
  alias Timemanager.TeamContext
  alias Timemanager.TeamContext.Team

  def list_wt_teams do
    Repo.all(WTTeam)
  end

  def get_wt_team!(id), do: Repo.get!(WTTeam, id)

  def create_wt_team(attrs \\ %{}) do
    %WTTeam{}
    |> WTTeam.changeset(attrs)
    |> Repo.insert()
  end

  def update_wt_team(%WTTeam{} = wt_team, attrs) do
    wt_team
    |> WTTeam.changeset(attrs)
    |> Repo.update()
  end

  def add_wt_project(attrs \\ %{}, team_id, wt_id, user_id) do
    {hour_worked, ""}  = Integer.parse(attrs["hour_worked"])

    query = from p in Team, where: p.id == ^team_id

    if Repo.exists?(query) do
      team = Repo.get_by!(Team, id: team_id)

      hours_remaining = team.hours_remaining - hour_worked

      hours_remaining = cond do
        hours_remaining < 0 ->
          0
        true ->
          team.hours_remaining - hour_worked
      end

      TeamContext.update_team(team, %{hours_remaining: hours_remaining})
    end

    %WTTeam{team_id: team_id, wt_id: wt_id, user_id: user_id}
    |> WTTeam.changeset(attrs)
    |> Repo.insert()


  end

  def delete_wt_team(%WTTeam{} = wt_team) do
    Repo.delete(wt_team)
  end

  def change_wt_team(%WTTeam{} = wt_team, attrs \\ %{}) do
    WTTeam.changeset(wt_team, attrs)
  end
end
