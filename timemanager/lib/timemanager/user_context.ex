defmodule Timemanager.UserContext do
  @moduledoc """
  The UserContext context.
  """

  import Ecto.Query, warn: false
  alias Timemanager.Repo

  alias Timemanager.UserContext.User

  def list_users do
    try do
      users = Repo.all(User) |> Repo.preload([:role, :clocks, :workingtimes])
      {:ok, users}
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def get_user(id) do
    try do
      user = Repo.get!(User, id) |> Repo.preload([:role, :clocks, :workingtimes])
      {:ok, user}
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def get_by_info(username, email) do
    try do
      user =
        Repo.get_by!(User, username: username, email: email)
        |> Repo.preload([:role, :clocks, :workingtimes])

      {:ok, user}
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def get_by_username(username) do
    try do
      user =
        Repo.get_by!(User, username: username) |> Repo.preload([:role, :clocks, :workingtimes])

      {:ok, user}
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def get_by_email(email) do
    try do
      user = Repo.get_by!(User, email: email) |> Repo.preload([:role, :clocks, :workingtimes])
      {:ok, user}
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def create_user(attrs \\ %{}) do
    try do
      # role = 1

      role =
        case attrs["role_id"] do
          nil -> 1
          _ -> attrs["role_id"]
        end

      %User{role_id: role}
      |> Repo.preload([:role])
      |> User.changeset(attrs)
      |> Repo.insert()
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def update_user(%User{} = user, attrs) do
    try do
      user
      |> Repo.preload([:role])
      |> User.changeset(attrs)
      |> Repo.update()
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def delete_user(%User{} = user) do
    try do
      Repo.delete(user)
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def change_user(%User{} = user, attrs \\ %{}) do
    try do
      user = User.changeset(user, attrs)
      {:ok, user}
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end
end
