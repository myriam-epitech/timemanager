defmodule Timemanager.TeamContext do
  @moduledoc """
  The TeamContext context.
  """

  import Ecto.Query, warn: false
  alias Timemanager.Repo

  alias Timemanager.TeamContext.Team
  alias Timemanager.UserTeamContext.UserTeam
  alias Timemanager.WTTeamContext.WTTeam
  alias Timemanager.WTContext
  @doc """
  Returns the list of teams.

  ## Examples

      iex> list_teams()
      [%Team{}, ...]

  """
  def list_teams do
    Repo.all(Team) |> Repo.preload([users: [:role]]) |> Repo.preload([workingtimes: [:user, :wt_type]])
  end

  @doc """
  Gets a single team.

  Raises `Ecto.NoResultsError` if the Team does not exist.

  ## Examples

      iex> get_team!(123)
      %Team{}

      iex> get_team!(456)
      ** (Ecto.NoResultsError)

  """
  def get_team!(id), do: Repo.get!(Team, id)|> Repo.preload([users: [:role]])


  def get_by_manager!(manager_id) do
    {manager_id, ""}  = Integer.parse(manager_id)
    query = from(p in UserTeam, where: [user_id: ^manager_id, is_manager: true])
    user_team =  Repo.all(query)
    data = for element <- user_team do
      team = get_team!(element.team_id) |> Repo.preload([users: [:role]]) |> Repo.preload([:workingtimes])

      final_users = for user <- team.users do
        user_team_users = Repo.get_by!(UserTeam, [user_id: user.id, team_id: team.id ])
        user = Map.put(user, :hour_worked, user_team_users.hour_worked)
        user = Map.put(user, :is_manager, user_team_users.is_manager)
        user
      end
      team = Map.put(team, :users, final_users)
      team
    end
    data
  end


  def get_teams_wt_by_user(user_id) do
    try do
      query_user_teams = from(p in UserTeam, where: [user_id: ^user_id])
      user_teams = Repo.all(query_user_teams)

      data = for element <- user_teams do
        team = get_team!(element.team_id) #|> Repo.preload([users: [:role]]) |> Repo.preload([:workingtimes])


        wt_query = from(p in WTTeam, where: [user_id: ^user_id, team_id: ^element.team_id])
        wt_teams = Repo.all(wt_query)
        workingtimes = for el <- wt_teams do
          { :ok, wt } = WTContext.get_wt(el.wt_id)
          wt = Map.put(wt, :hour_worked, el.hour_worked)
          wt
        end
        team = Map.put(team, :workingtimes, workingtimes)

        team
      end
      {:ok, data}
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  @doc """
  Creates a team.

  ## Examples

      iex> create_team(%{field: value})
      {:ok, %Team{}}

      iex> create_team(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_team(attrs \\ %{}) do
    %Team{}
    |> Repo.preload([users: [:role]]) |> Repo.preload([:workingtimes])
    |> Team.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a team.

  ## Examples

      iex> update_team(team, %{field: new_value})
      {:ok, %Team{}}

      iex> update_team(team, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_team(%Team{} = team, attrs) do
    team
    |> Repo.preload([users: [:role]]) |> Repo.preload([:workingtimes])
    |> Team.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a team.

  ## Examples

      iex> delete_team(team)
      {:ok, %Team{}}

      iex> delete_team(team)
      {:error, %Ecto.Changeset{}}

  """
  def delete_team(%Team{} = team) do
    Repo.delete(team)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking team changes.

  ## Examples

      iex> change_team(team)
      %Ecto.Changeset{data: %Team{}}

  """
  def change_team(%Team{} = team, attrs \\ %{}) do
    Team.changeset(team, attrs)
  end
end
