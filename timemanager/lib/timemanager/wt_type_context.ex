defmodule Timemanager.WTTypeContext do
  @moduledoc """
  The WTTypeContext context.
  """

  import Ecto.Query, warn: false
  alias Timemanager.Repo

  alias Timemanager.WTTypeContext.WTType

  @doc """
  Returns the list of wt_types.

  ## Examples

      iex> list_wt_types()
      [%WTType{}, ...]

  """
  def list_wt_types do
    Repo.all(WTType)
  end

  @doc """
  Gets a single wt_type.

  Raises `Ecto.NoResultsError` if the Wt type does not exist.

  ## Examples

      iex> get_wt_type!(123)
      %WTType{}

      iex> get_wt_type!(456)
      ** (Ecto.NoResultsError)

  """
  def get_wt_type!(id), do: Repo.get!(WTType, id)

  @doc """
  Creates a wt_type.

  ## Examples

      iex> create_wt_type(%{field: value})
      {:ok, %WTType{}}

      iex> create_wt_type(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_wt_type(attrs \\ %{}) do
    %WTType{}
    |> WTType.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a wt_type.

  ## Examples

      iex> update_wt_type(wt_type, %{field: new_value})
      {:ok, %WTType{}}

      iex> update_wt_type(wt_type, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_wt_type(%WTType{} = wt_type, attrs) do
    wt_type
    |> WTType.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a wt_type.

  ## Examples

      iex> delete_wt_type(wt_type)
      {:ok, %WTType{}}

      iex> delete_wt_type(wt_type)
      {:error, %Ecto.Changeset{}}

  """
  def delete_wt_type(%WTType{} = wt_type) do
    Repo.delete(wt_type)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking wt_type changes.

  ## Examples

      iex> change_wt_type(wt_type)
      %Ecto.Changeset{data: %WTType{}}

  """
  def change_wt_type(%WTType{} = wt_type, attrs \\ %{}) do
    WTType.changeset(wt_type, attrs)
  end
end
