defmodule Timemanager.ChartContext.Chart do
  use Ecto.Schema
  import Ecto.Changeset

  schema "charts" do
    field :user_id, :integer
    field :positionX, :float
    field :positionY, :float
    field :settings, :map
    field :type, :string

    timestamps()
  end

  @doc false
  def changeset(chart, attrs) do
    chart
    |> cast(attrs, [:user_id, :positionX, :positionY, :type, :settings])
    |> validate_required([:user_id, :positionX, :positionY, :type, :settings])
  end
end
