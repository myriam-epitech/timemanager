defmodule Timemanager.UserTeamContext do
  @moduledoc """
  The UserTeamContext context.
  """

  import Ecto.Query, warn: false
  alias Timemanager.Repo

  alias Timemanager.UserTeamContext.UserTeam
  alias Timemanager.UserContext.User
  alias Timemanager.UserContext

  def list_users_teams do
    Repo.all(UserTeam)
  end

  def get_user_team!(id), do: Repo.get!(UserTeam, id)

  def create_user_team(attrs \\ %{}) do
    %UserTeam{}
    |> UserTeam.changeset(attrs)
    |> Repo.insert()
  end

  def add_user_to_team(userID, id) do
    try do
      %UserTeam{user_id: userID, team_id: id}
      |> Repo.insert()

    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def add_manager_to_team(is_manager, userID, id) do
    # try do
      query_user_team = from(p in UserTeam, where: p.team_id == ^id, where: p.user_id == ^userID)
      user_team = Repo.one(query_user_team)

      if user_team do
        attrUserTeam = %{is_manager: is_manager}
        update_res = update_user_team(user_team, attrUserTeam)
      else
        %UserTeam{user_id: userID, team_id: id, is_manager: is_manager}
        |> Repo.insert()
      end

      # Si is_manager est true alors vérifie si le user a déjà le role de manager sinon, l'ajoute
      if(is_manager) do
        query = from(p in User, where: p.id == ^userID)
        user = Repo.one(query)

        if user.role_id < 2 do
          attrUser = %{
            role_id: 2
          }

          UserContext.update_user(user, attrUser)
        end
      end
      user_team
    #   {:ok, user_team}
    # rescue
    #   _e in Ecto.NoResultsError -> {:error, :not_found}
    #   _e -> {:error, :other_errors}
    # end
  end

  def update_user_team(%UserTeam{} = user_team, attrs) do
    user_team
    |> UserTeam.changeset(attrs)
    |> Repo.update()
  end

  def remove_user_from_team(id, userID) do
    try do
      user_team = Repo.get_by!(UserTeam, user_id: userID, team_id: id)
      Repo.delete(user_team)
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def delete_user_team(%UserTeam{} = user_team) do
    Repo.delete(user_team)
  end

  def change_user_team(%UserTeam{} = user_team, attrs \\ %{}) do
    UserTeam.changeset(user_team, attrs)
  end

  def get_users_managed(id) do
    try do
      query = from(p in UserTeam, where: [user_id: ^id, is_manager: true])
      user_teams = Repo.all(query)

      if length(user_teams) > 0 do
        teams_id = for team <- user_teams, do: team.team_id
        query = from(p in UserTeam, where: p.team_id in ^teams_id, where: p.is_manager == false)
        all_u_t = Repo.all(query)

        users_id = for u_t <- all_u_t, do: u_t.user_id
        users_query = from(p in User, where: p.id in ^users_id)
        users = Repo.all(users_query) |> Repo.preload([:role, :clocks, :workingtimes])
        {:ok, users}
      else
        {:ok, user_teams}
      end
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end
end
