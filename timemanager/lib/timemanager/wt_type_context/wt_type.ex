defmodule Timemanager.WTTypeContext.WTType do
  use Ecto.Schema
  import Ecto.Changeset

  schema "wt_types" do
    field :name, :string
    has_many :workingtimes, Timemanager.WTContext.WT

    timestamps()
  end

  @doc false
  def changeset(wt_type, attrs) do
    wt_type
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> unique_constraint([:name])
  end
end
