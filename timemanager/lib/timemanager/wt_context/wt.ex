defmodule Timemanager.WTContext.WT do
  use Ecto.Schema
  import Ecto.Changeset

  schema "workingtimes" do
    field :end, :naive_datetime
    field :start, :naive_datetime
    field :comments, :string
    field :day_hours, :float
    field :night_hours, :float, default: 0.0
    belongs_to :user, Timemanager.UserContext.User
    belongs_to :wt_type, Timemanager.WTTypeContext.WTType

    many_to_many :teams, Timemanager.TeamContext.Team, join_through: "wt_teams"

    timestamps()
  end

  @doc false
  def changeset(wt, attrs) do
    wt
    |> cast(attrs, [:start, :end, :user_id, :wt_type_id, :comments, :day_hours, :night_hours])
    |> validate_required([:start, :end, :user_id, :wt_type_id, :day_hours])
    |> assoc_constraint(:user)
  end
end
