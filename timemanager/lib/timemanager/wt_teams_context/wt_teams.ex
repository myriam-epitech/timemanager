defmodule Timemanager.WTTeamContext.WTTeam do
  use Ecto.Schema
  import Ecto.Changeset

  schema "wt_teams" do
    field :hour_worked, :float
    field :team_id, :integer
    field :wt_id, :integer
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(wt_teams, attrs) do
    wt_teams
    |> cast(attrs, [:wt_id, :team_id, :user_id, :hour_worked])
    |> validate_required([:wt_id, :team_id, :user_id, :hour_worked])
  end
end
