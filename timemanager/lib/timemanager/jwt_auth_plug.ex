defmodule Timemanager.JWTAuthPlug do
  import Plug.Conn
  import Phoenix.Controller
  alias Timemanager.UserContext
  alias Timemanager.UserContext.User

  def init(opts), do: opts

  def call(conn, _opts) do
    token = get_token_header(conn)

    signer =
      Joken.Signer.create(
        "HS256",
        "v5ZUl4SnVIiV6a4O3TeEJnI4OwhW9cJc5vNPvOdIyEOty9t4pVYfDXLq6rPf1PXQ"
        )

        case TimemanagerWeb.JWTToken.verify_and_validate(token, signer) do
          {:ok, claims} ->
            conn
            |> ok(claims, token)
            |> isUserPermitted()

            {:error, _error} ->
              conn |> unauthorized
    end
  end

  defp get_token_header(conn) do
    bearer = get_req_header(conn, "authorization") |> List.first()

    if bearer == nil do
      conn |> unauthorized
    end

    bearer |> String.split(" ") |> List.last()
  end

  defp ok(conn, token_payload, token) do
    assign(conn, :user_id, token_payload["user_id"])
    |> assign(:jwt, token)
    |> assign(:role, token_payload["role"])
  end

  defp unauthorized(conn) do
    conn
    |> put_status(401)
    |> put_view(TimemanagerWeb.ErrorView)
    |> render("error.json", %{status: 1, message: "Unauthorized. Connection required."})
    |> halt
  end

  defp forbidden(conn) do
    conn
    |> put_status(403)
    |> put_view(TimemanagerWeb.ErrorView)
    |> render("error.json", %{status: 1, message: "Forbidden. Don't have the right to access"})
    |> halt
  end

  def isUserPermitted(conn) do
    method = conn.method
    user_id = conn.assigns.user_id
    role = conn.assigns.role

    with {:ok, users_managed} <- Timemanager.UserTeamContext.get_users_managed(user_id) do
      users_managed_id = for user <- users_managed, do: user.id

      paths =
        for path <- conn.path_info do
          case Integer.parse(path) do
            {integer, ""} -> definePath(integer, user_id, users_managed_id)
            _ -> path
          end
        end

      request_name = method <> "." <> Enum.join(paths, ".")

      request_name =
        cond do
          String.contains?(request_name, ["PUT.api.workingtimes"]) ->
            wt_id = Enum.at(conn.path_info, 2)
            {:ok, wt} = Timemanager.WTContext.get_wt(wt_id)
            owner_wt_id = wt.user_id
            "PUT.api.workingtimes" <> "." <> definePath(owner_wt_id, user_id, users_managed_id)

          String.contains?(request_name, ["DELETE.api.workingtimes"]) ->
            wt_id = Enum.at(conn.path_info, 2)
            {:ok, wt} = Timemanager.WTContext.get_wt(wt_id)
            owner_wt_id = wt.user_id
            "DELETE.api.workingtimes" <> "." <> definePath(owner_wt_id, user_id, users_managed_id)

          String.contains?(request_name, ["POST.api.users.employee.teams"]) ->
            wt_id = Enum.at(conn.path_info, 2)
            {:ok, wt} = Timemanager.WTContext.get_wt(wt_id)
            owner_wt_id = wt.user_id

            "POST.api.declareprojecthoursonwt.employee" <>
              "." <> definePath(owner_wt_id, user_id, users_managed_id)

          String.contains?(request_name, ["POST.api.users.manager.teams"]) ->
            wt_id = Enum.at(conn.path_info, 2)
            {:ok, wt} = Timemanager.WTContext.get_wt(wt_id)
            owner_wt_id = wt.user_id

            "POST.api.declareprojecthoursonwt.manager" <>
              "." <> definePath(owner_wt_id, user_id, users_managed_id)

          String.contains?(request_name, ["POST.api.users.other.teams"]) ->
            wt_id = Enum.at(conn.path_info, 2)
            {:ok, wt} = Timemanager.WTContext.get_wt(wt_id)
            owner_wt_id = wt.user_id

            "POST.api.declareprojecthoursonwt.other" <>
              "." <> definePath(owner_wt_id, user_id, users_managed_id)

          String.contains?(request_name, ["PUT.api.teams.employee"]) ->
            team_id = Enum.at(conn.path_info, 2)
            user_id = "#{user_id}"
            teams = Timemanager.TeamContext.get_by_manager!(user_id)
            manager_of_teams_id = for team <- teams, do: team.id
            "PUT.api.editteam" <> "." <> definePath(nil, team_id, manager_of_teams_id)

          String.contains?(request_name, ["PUT.api.teams.manager"]) ->
            team_id = Enum.at(conn.path_info, 2)
            user_id = "#{user_id}"

            teams = Timemanager.TeamContext.get_by_manager!(user_id)
            manager_of_teams_id = for team <- teams, do: team.id
            "PUT.api.editteam" <> "." <> definePath(nil, team_id, manager_of_teams_id)

          String.contains?(request_name, ["PUT.api.teams.other"]) ->
            team_id = Enum.at(conn.path_info, 2)
            user_id = "#{user_id}"

            teams = Timemanager.TeamContext.get_by_manager!(user_id)
            manager_of_teams_id = for team <- teams, do: team.id
            "PUT.api.editteam" <> "." <> definePath(nil, team_id, manager_of_teams_id)

          true ->
            method <> "." <> Enum.join(paths, ".")
        end

      has_permission = is_permitted(request_name, role)

      if has_permission, do: conn, else: conn |> forbidden
    end
  end

  defp definePath(integer, user_id, users_managed_id) do
    cond do
      integer == user_id ->
        "employee"

      Enum.find(users_managed_id, fn map -> map == integer end) ->
        "manager"

      true ->
        "other"
    end
  end

  defp is_permitted(request_name, role) do
    if is_nil(requests_list()[request_name]) do
      role >= 3
    else
      role >= requests_list()[request_name]
    end
  end

  defp requests_list do
    %{
      "GET.api.currentUser" => 1,
      # CLOCKS

      # GET     /api/users                     TimemanagerWeb.UserController :index
      "GET.api.users" => 3,
      # GET     /api/users/:id                 TimemanagerWeb.UserController :show
      "GET.api.users.employee" => 1,
      "GET.api.users.manager" => 2,
      "GET.api.users.other" => 3,
      # POST    /api/users                     TimemanagerWeb.UserController :create
      "POST.api.users" => 3,
      # PUT     /api/users/:id                 TimemanagerWeb.UserController :update
      "PUT.api.users.employee" => 1,
      "PATCH.api.users.employee" => 1,
      "PUT.api.users.manager" => 2,
      "PATCH.api.users.manager" => 2,
      "PUT.api.users.other" => 3,
      "PATCH.api.users.other" => 3,
      # DELETE  /api/users/:id                 TimemanagerWeb.UserController :delete
      "DELETE.api.users.employee" => 1,
      "DELETE.api.users.manager" => 3,
      "DELETE.api.users.other" => 3,
      # PUT     /api/workingtimes/:id          TimemanagerWeb.WTController :update
      "PUT.api.workingtimes.employee" => 1,
      "PUT.api.workingtimes.manager" => 2,
      "PUT.api.workingtimes.other" => 3,
      # DELETE  /api/workingtimes/:id          TimemanagerWeb.WTController :delete
      "DELETE.api.workingtimes.employee" => 1,
      "DELETE.api.workingtimes.manager" => 2,
      "DELETE.api.workingtimes.other" => 3,
      # GET     /api/workingtimes/:userID      TimemanagerWeb.WTController :getAll
      "GET.api.workingtimes.employee" => 1,
      "GET.api.workingtimes.manager" => 2,
      "GET.api.workingtimes.other" => 3,
      # GET     /api/workingtimes/:userID/:id  TimemanagerWeb.WTController :getOne
      "GET.api.workingtimes.employee.employee" => 1,
      "GET.api.workingtimes.employee.manager" => 1,
      "GET.api.workingtimes.employee.other" => 1,
      "GET.api.workingtimes.manager.employee" => 2,
      "GET.api.workingtimes.manager.manager" => 2,
      "GET.api.workingtimes.manager.other" => 2,
      "GET.api.workingtimes.other.employee" => 3,
      "GET.api.workingtimes.other.manager" => 3,
      "GET.api.workingtimes.other.other" => 3,
      # POST    /api/workingtimes/:userID      TimemanagerWeb.WTController :create
      "POST.api.workingtimes.employee" => 1,
      "POST.api.workingtimes.manager" => 2,
      "POST.api.workingtimes.other" => 3,
      # GET     /api/clocks/:userID            TimemanagerWeb.ClockController :getByUserID
      "GET.api.clocks.employee" => 1,
      "GET.api.clocks.manager" => 1,
      "GET.api.clocks.other" => 1,
      # POST    /api/clocks/:userID            TimemanagerWeb.ClockController :create
      "POST.api.clocks.employee" => 1,
      "POST.api.clocks.manager" => 2,
      "POST.api.clocks.other" => 3,
      # POST    /api/teams                     TimemanagerWeb.TeamController :create
        # role doit être super manager pour pouvoir créer une team
      # PUT     /api/teams/:id                 TimemanagerWeb.TeamController :update
      "POST.api.editteam.employee" => 3,
      "POST.api.editteam.manager" => 2,
      "POST.api.editteam.other" => 3,
      # DELETE  /api/teams/:id                 TimemanagerWeb.TeamController :delete
          #role doit être super manager pour créer une team
            # "DELETE.api.teams.employee" => 3,
            # "DELETE.api.teams.manager" => 3,
            # "DELETE.api.teams.other" => 3,
      # GET     /api/teams                     TimemanagerWeb.TeamController :index
      # GET     /api/teams/manager/:managerID  TimemanagerWeb.TeamController :getByManager
      "GET.api.teams.manager.employee" => 2,
      "GET.api.teams.manager.manager" => 2,
      "GET.api.teams.manager.other" => 3,
      # GET     /api/projects/:userID          TimemanagerWeb.TeamController :getProjectsByUser
      "GET.api.projects.employee" => 1,
      "GET.api.projects.manager" => 2,
      "GET.api.projects.other" => 3,
      # GET     /api/users/manager/:managerID  TimemanagerWeb.UserTeamController :getUsersManaged
      "GET.api.users.manager.employee" => 2,
      "GET.api.users.manager.manager" => 2,
      "GET.api.users.manager.other" => 3,
      # DELETE  /api/teams/:userID/:id         TimemanagerWeb.UserTeamController :removeUserFromTeam
      "DELETE.api.teams.employee.employee" => 1,
      "DELETE.api.teams.employee.manager" => 1,
      "DELETE.api.teams.employee.other" => 1,
      "DELETE.api.teams.manager.employee" => 2,
      "DELETE.api.teams.manager.manager" => 2,
      "DELETE.api.teams.manager.other" => 2,
      "DELETE.api.teams.other.employee" => 3,
      "DELETE.api.teams.other.manager" => 3,
      "DELETE.api.teams.other.other" => 3,
      # POST    /api/teams/:userID/:id         TimemanagerWeb.UserTeamController :addUserToTeam
      "POST.api.teams.employee.employee" => 1,
      "POST.api.teams.employee.manager" => 1,
      "POST.api.teams.employee.other" => 1,
      "POST.api.teams.manager.employee" => 2,
      "POST.api.teams.manager.manager" => 2,
      "POST.api.teams.manager.other" => 2,
      "POST.api.teams.other.employee" => 2,
      "POST.api.teams.other.manager" => 2,
      "POST.api.teams.other.other" => 2,
      # GET     /api/charts                    TimemanagerWeb.ChartController :index
      "GET.charts" => 3,
      # GET     /api/charts/:id                TimemanagerWeb.ChartController :show
      "GET.charts.employee" => 1,
      "GET.charts.manager" => 2,
      "GET.charts.other" => 3,
      # POST    /api/charts                    TimemanagerWeb.ChartController :create
      "POST.charts" => 2,
      # PUT     /api/charts/:id                TimemanagerWeb.ChartController :update
      # DELETE  /api/charts/:id                TimemanagerWeb.ChartController :delete
      # POST    /api/users/:userID/teams/:idTeam/wt/:idWT    TimemanagerWeb.WTTeamController :addWTProject
      "POST.api.declareprojecthoursonwt.employee" => 1,
      "POST.api.declareprojecthoursonwt.manager" => 2,
      "POST.api.declareprojecthoursonwt.other" => 3,
      # GET     /api/wages/:userID             TimemanagerWeb.WageController :getAll
      # GET     /api/logout                    TimemanagerWeb.AuthController :logout
      "GET.api.logout" => 1
    }
  end
end
