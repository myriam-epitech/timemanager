defmodule Timemanager.WageContext do
  @moduledoc """
  The WageContext context.
  """

  import Ecto.Query, warn: false
  alias Timemanager.Repo

  alias Timemanager.WageContext.Wage

  @doc """
  Returns the list of wages.

  ## Examples

      iex> list_wages()
      [%Wage{}, ...]

  """
  def list_wages do
    Repo.all(Wage)
  end

  @doc """
  Gets a single wage.

  Raises `Ecto.NoResultsError` if the Wage does not exist.

  ## Examples

      iex> get_wage!(123)
      %Wage{}

      iex> get_wage!(456)
      ** (Ecto.NoResultsError)

  """
  def get_wage!(id) do
    query = from(w in Wage, where: w.user_id == ^id)
    Repo.all(query) 
    # |> Repo.preload([user: [:role]])
    # Repo.get!(Wage, id)
    # |> Repo.preload([user: [:role]])
  end

  @doc """
  Creates a wage.

  ## Examples

      iex> create_wage(%{field: value})
      {:ok, %Wage{}}

      iex> create_wage(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_wage(attrs \\ %{}) do
    %Wage{}
    |> Repo.preload([user: [:role]])
    |> Wage.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a wage.

  ## Examples

      iex> update_wage(wage, %{field: new_value})
      {:ok, %Wage{}}

      iex> update_wage(wage, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_wage(%Wage{} = wage, attrs) do
    wage
    |> Wage.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a wage.

  ## Examples

      iex> delete_wage(wage)
      {:ok, %Wage{}}

      iex> delete_wage(wage)
      {:error, %Ecto.Changeset{}}

  """
  def delete_wage(%Wage{} = wage) do
    Repo.delete(wage)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking wage changes.

  ## Examples

      iex> change_wage(wage)
      %Ecto.Changeset{data: %Wage{}}

  """
  def change_wage(%Wage{} = wage, attrs \\ %{}) do
    Wage.changeset(wage, attrs)
  end
end
