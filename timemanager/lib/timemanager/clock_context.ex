defmodule Timemanager.ClockContext do
  import Ecto.Query, warn: false
  alias Timemanager.Repo
  alias Timemanager.ClockContext.Clock

  def list_clocks do
    Repo.all(Clock)
  end

  def getByUserID(user_id) do
    try do
      query = from(p in Clock, where: p.user_id == ^user_id)
      clock = Repo.one(query) |> Repo.preload(user: [:role])

      _result =
        case clock do
          nil -> {:error, :not_found}
          _ -> {:ok, clock}
        end
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def get_clock(id), do: Repo.get!(Clock, id) |> Repo.preload(user: [:role])

  def create_clock(attrs \\ %{}, user_id) do
    try do
      query = from p in Clock, where: p.user_id == ^user_id

      if Repo.exists?(query) do
        clock = Repo.get_by!(Clock, user_id: user_id)

        if attrs["status"] == false and clock.status do
          newWT = %{
            start: clock.time,
            end: attrs["time"],
            day_hours: attrs["day_hours"],
            night_hours: attrs["night_hours"],
            wt_type_id: attrs["wt_type_id"]
          }

          Timemanager.WTContext.create_wt(newWT, user_id)
        end

        update_clock(clock, attrs)

      else
          %Clock{user_id: user_id}
          |> Repo.preload(user: [:role])
          |> Clock.changeset(attrs)
          |> Repo.insert()
      end
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def update_clock(%Clock{} = clock, attrs) do
    clock
    |> Repo.preload(user: [:role])
    |> Clock.changeset(attrs)
    |> Repo.update()
  end

  def delete_clock(%Clock{} = clock) do
    Repo.delete(clock)
  end

  def change_clock(%Clock{} = clock, attrs \\ %{}) do
    Clock.changeset(clock, attrs)
  end
end
