defmodule Timemanager.UserTeamContext.UserTeam do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users_teams" do
    field :user_id, :integer
    field :team_id, :integer
    field :hour_worked, :float, default: 0.0
    field :is_manager, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(user_team, attrs) do
    user_team
    |> cast(attrs, [:user_id, :team_id, :hour_worked, :is_manager])
    |> validate_required([:user_id, :team_id, :hour_worked])
  end
end
