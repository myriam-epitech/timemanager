defmodule Timemanager.TeamContext.Team do
  use Ecto.Schema
  import Ecto.Changeset

  schema "teams" do
    field :name, :string
    field :total_hours, :float, default: 0.0
    field :hours_remaining, :float, default: 0.0

    many_to_many :users, Timemanager.UserContext.User, join_through: "users_teams"
    many_to_many :workingtimes, Timemanager.WTContext.WT, join_through: "wt_teams"

    timestamps()
  end

  @doc false
  def changeset(team, attrs) do
    team
    |> cast(attrs, [:name, :total_hours, :hours_remaining])
    |> validate_required([:name, :total_hours])
  end
end
