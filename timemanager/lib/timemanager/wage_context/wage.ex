defmodule Timemanager.WageContext.Wage do
  use Ecto.Schema
  import Ecto.Changeset

  schema "wages" do
    field :value, :float
    field :month, :naive_datetime

    belongs_to :user, Timemanager.UserContext.User

    timestamps()
  end

  @doc false
  def changeset(wage, attrs) do
    wage
    |> cast(attrs, [:value, :user_id, :month])
    |> validate_required([:value, :user_id, :month])
  end
end
