defmodule Timemanager.WTContext do
  @moduledoc """
  The WTContext context.
  """

  import Ecto.Query, warn: false
  import Ecto.Adapters.Postgres
  alias Timemanager.Repo

  alias Timemanager.WTContext.WT

  def list_workingtimes do
    Repo.all(WT)
  end

  def getFirst!(user_id) do
    try do
    query =
      from w in WT,
        where: w.user_id == ^user_id,
        order_by: [asc: w.start],
        limit: 1

    result = Repo.one(query)

    case result do
      nil -> {:error, :not_found}
      _ -> {:ok, result}
    end
  rescue
    _e in Ecto.NoResultsError -> {:error, :not_found}
    _e -> {:error, :other_errors}
  end
  end

  def getLast!(user_id) do
    try do
      query =
        from w in WT,
          where: w.user_id == ^user_id,
          order_by: [desc: w.end],
          limit: 1

      result = Repo.one(query)

      case result do
        nil -> {:error, :not_found}
        _ -> {:ok, result}
      end
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def get_wt(id) do
    try do
      wt = Repo.get!(WT, id) |> Repo.preload(user: [:role]) |> Repo.preload(:wt_type)
      {:ok, wt}
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def getAllByUserID(user_id) do
    try do
      query = from(p in WT, where: p.user_id == ^user_id)
      wt = Repo.all(query) |> Repo.preload(user: [:role]) |> Repo.preload(:wt_type)

      # _result =
      #   case length(wt) do
      #     0 -> {:error, :not_found}
      #     _ ->
            {:ok, wt}
        # end
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def getAllByUserIDAndParams(user_id, param_start, param_end) do
    try do
      query =
        from p in WT,
          where: p.user_id == ^user_id,
          where: p.end <= ^param_end and p.start >= ^param_start

      wt = Repo.all(query) |> Repo.preload(user: [:role]) |> Repo.preload(:wt_type)

      # _result =
      #   case length(wt) do
      #     0 -> {:error, :no_content}
      #     _ ->
            {:ok, wt}
        # end
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def getOneByUserID(user_id, id) do
    try do
      wt =
        Repo.get_by!(WT, id: id, user_id: user_id)
        |> Repo.preload(user: [:role])
        |> Repo.preload(:wt_type)

      {:ok, wt}
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def create_wt(attrs \\ %{}, user_id) do
    try do
      %WT{user_id: user_id}
      |> Repo.preload(:wt_type)
      |> Repo.preload(user: [:role])
      |> WT.changeset(attrs)
      |> Repo.insert()
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def update_wt(%WT{} = wt, attrs) do
    try do
      wt
      |> Repo.preload(user: [:role])
      |> Repo.preload(:wt_type)
      |> WT.changeset(attrs)
      |> Repo.update()
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def delete_wt(%WT{} = wt) do
    try do
      _wt = Repo.delete(wt)
    rescue
      _e in Ecto.NoResultsError -> {:error, :not_found}
      _e -> {:error, :other_errors}
    end
  end

  def change_wt(%WT{} = wt, attrs \\ %{}) do
    WT.changeset(wt, attrs)
  end
end
