# defmodule Timemanager.WTTeamContextTest do
#   use Timemanager.DataCase

#   alias Timemanager.WTTeamContext

#   describe "wt_teams" do
#     alias Timemanager.WTTeamContext.WTTeam

#     import Timemanager.WTTeamContextFixtures

#     @invalid_attrs %{hour_worked: nil, team_id: nil, wt_id: nil}

#     test "list_wt_teams/0 returns all wt_teams" do
#       wt_team = wt_team_fixture()
#       assert WTTeamContext.list_wt_teams() == [wt_team]
#     end

#     test "get_wt_team!/1 returns the wt_team with given id" do
#       wt_team = wt_team_fixture()
#       assert WTTeamContext.get_wt_team!(wt_team.id) == wt_team
#     end

#     test "create_wt_team/1 with valid data creates a wt_team" do
#       valid_attrs = %{hour_worked: 120.5, team_id: 42, wt_id: 42}

#       assert {:ok, %WTTeam{} = wt_team} = WTTeamContext.create_wt_team(valid_attrs)
#       assert wt_team.hour_worked == 120.5
#       assert wt_team.team_id == 42
#       assert wt_team.wt_id == 42
#     end

#     test "create_wt_team/1 with invalid data returns error changeset" do
#       assert {:error, %Ecto.Changeset{}} = WTTeamContext.create_wt_team(@invalid_attrs)
#     end

#     test "update_wt_team/2 with valid data updates the wt_team" do
#       wt_team = wt_team_fixture()
#       update_attrs = %{hour_worked: 456.7, team_id: 43, wt_id: 43}

#       assert {:ok, %WTTeam{} = wt_team} = WTTeamContext.update_wt_team(wt_team, update_attrs)
#       assert wt_team.hour_worked == 456.7
#       assert wt_team.team_id == 43
#       assert wt_team.wt_id == 43
#     end

#     test "update_wt_team/2 with invalid data returns error changeset" do
#       wt_team = wt_team_fixture()
#       assert {:error, %Ecto.Changeset{}} = WTTeamContext.update_wt_team(wt_team, @invalid_attrs)
#       assert wt_team == WTTeamContext.get_wt_team!(wt_team.id)
#     end

#     test "delete_wt_team/1 deletes the wt_team" do
#       wt_team = wt_team_fixture()
#       assert {:ok, %WTTeam{}} = WTTeamContext.delete_wt_team(wt_team)
#       assert_raise Ecto.NoResultsError, fn -> WTTeamContext.get_wt_team!(wt_team.id) end
#     end

#     test "change_wt_team/1 returns a wt_team changeset" do
#       wt_team = wt_team_fixture()
#       assert %Ecto.Changeset{} = WTTeamContext.change_wt_team(wt_team)
#     end
#   end
# end
