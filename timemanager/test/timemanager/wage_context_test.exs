# defmodule Timemanager.WageContextTest do
#   use Timemanager.DataCase

#   alias Timemanager.WageContext

#   describe "wages" do
#     alias Timemanager.WageContext.Wage

#     import Timemanager.WageContextFixtures

#     @invalid_attrs %{user_id: nil, value: nil}

#     test "list_wages/0 returns all wages" do
#       wage = wage_fixture()
#       assert WageContext.list_wages() == [wage]
#     end

#     test "get_wage!/1 returns the wage with given id" do
#       wage = wage_fixture()
#       assert WageContext.get_wage!(wage.id) == wage
#     end

#     test "create_wage/1 with valid data creates a wage" do
#       valid_attrs = %{user_id: 42, value: 120.5}

#       assert {:ok, %Wage{} = wage} = WageContext.create_wage(valid_attrs)
#       assert wage.user_id == 42
#       assert wage.value == 120.5
#     end

#     test "create_wage/1 with invalid data returns error changeset" do
#       assert {:error, %Ecto.Changeset{}} = WageContext.create_wage(@invalid_attrs)
#     end

#     test "update_wage/2 with valid data updates the wage" do
#       wage = wage_fixture()
#       update_attrs = %{user_id: 43, value: 456.7}

#       assert {:ok, %Wage{} = wage} = WageContext.update_wage(wage, update_attrs)
#       assert wage.user_id == 43
#       assert wage.value == 456.7
#     end

#     test "update_wage/2 with invalid data returns error changeset" do
#       wage = wage_fixture()
#       assert {:error, %Ecto.Changeset{}} = WageContext.update_wage(wage, @invalid_attrs)
#       assert wage == WageContext.get_wage!(wage.id)
#     end

#     test "delete_wage/1 deletes the wage" do
#       wage = wage_fixture()
#       assert {:ok, %Wage{}} = WageContext.delete_wage(wage)
#       assert_raise Ecto.NoResultsError, fn -> WageContext.get_wage!(wage.id) end
#     end

#     test "change_wage/1 returns a wage changeset" do
#       wage = wage_fixture()
#       assert %Ecto.Changeset{} = WageContext.change_wage(wage)
#     end
#   end
# end
