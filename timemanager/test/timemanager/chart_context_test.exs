# defmodule Timemanager.ChartContextTest do
#   use Timemanager.DataCase

#   alias Timemanager.ChartContext

#   describe "charts" do
#     alias Timemanager.ChartContext.Chart

#     import Timemanager.ChartContextFixtures

#     @invalid_attrs %{user_id: nil, positionX: nil, positionY: nil, settings: nil, type: nil}

#     test "list_charts/0 returns all charts" do
#       chart = chart_fixture()
#       assert ChartContext.list_charts() == [chart]
#     end

#     test "get_chart!/1 returns the chart with given id" do
#       chart = chart_fixture()
#       assert ChartContext.get_chart!(chart.id) == chart
#     end

#     test "create_chart/1 with valid data creates a chart" do
#       valid_attrs = %{user_id: 42, positionX: 120.5, positionY: 120.5, settings: %{}, type: "some type"}

#       assert {:ok, %Chart{} = chart} = ChartContext.create_chart(valid_attrs)
#       assert chart.user_id == 42
#       assert chart.positionX == 120.5
#       assert chart.positionY == 120.5
#       assert chart.settings == %{}
#       assert chart.type == "some type"
#     end

#     test "create_chart/1 with invalid data returns error changeset" do
#       assert {:error, %Ecto.Changeset{}} = ChartContext.create_chart(@invalid_attrs)
#     end

#     test "update_chart/2 with valid data updates the chart" do
#       chart = chart_fixture()
#       update_attrs = %{user_id: 43, positionX: 456.7, positionY: 456.7, settings: %{}, type: "some updated type"}

#       assert {:ok, %Chart{} = chart} = ChartContext.update_chart(chart, update_attrs)
#       assert chart.user_id == 43
#       assert chart.positionX == 456.7
#       assert chart.positionY == 456.7
#       assert chart.settings == %{}
#       assert chart.type == "some updated type"
#     end

#     test "update_chart/2 with invalid data returns error changeset" do
#       chart = chart_fixture()
#       assert {:error, %Ecto.Changeset{}} = ChartContext.update_chart(chart, @invalid_attrs)
#       assert chart == ChartContext.get_chart!(chart.id)
#     end

#     test "delete_chart/1 deletes the chart" do
#       chart = chart_fixture()
#       assert {:ok, %Chart{}} = ChartContext.delete_chart(chart)
#       assert_raise Ecto.NoResultsError, fn -> ChartContext.get_chart!(chart.id) end
#     end

#     test "change_chart/1 returns a chart changeset" do
#       chart = chart_fixture()
#       assert %Ecto.Changeset{} = ChartContext.change_chart(chart)
#     end
#   end
# end
