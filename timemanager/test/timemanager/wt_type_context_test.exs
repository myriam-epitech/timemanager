# defmodule Timemanager.WTTypeContextTest do
#   use Timemanager.DataCase

#   alias Timemanager.WTTypeContext

#   describe "wt_types" do
#     alias Timemanager.WTTypeContext.WTType

#     import Timemanager.WTTypeContextFixtures

#     @invalid_attrs %{name: nil}

#     test "list_wt_types/0 returns all wt_types" do
#       wt_type = wt_type_fixture()
#       assert WTTypeContext.list_wt_types() == [wt_type]
#     end

#     test "get_wt_type!/1 returns the wt_type with given id" do
#       wt_type = wt_type_fixture()
#       assert WTTypeContext.get_wt_type!(wt_type.id) == wt_type
#     end

#     test "create_wt_type/1 with valid data creates a wt_type" do
#       valid_attrs = %{name: "some name"}

#       assert {:ok, %WTType{} = wt_type} = WTTypeContext.create_wt_type(valid_attrs)
#       assert wt_type.name == "some name"
#     end

#     test "create_wt_type/1 with invalid data returns error changeset" do
#       assert {:error, %Ecto.Changeset{}} = WTTypeContext.create_wt_type(@invalid_attrs)
#     end

#     test "update_wt_type/2 with valid data updates the wt_type" do
#       wt_type = wt_type_fixture()
#       update_attrs = %{name: "some updated name"}

#       assert {:ok, %WTType{} = wt_type} = WTTypeContext.update_wt_type(wt_type, update_attrs)
#       assert wt_type.name == "some updated name"
#     end

#     test "update_wt_type/2 with invalid data returns error changeset" do
#       wt_type = wt_type_fixture()
#       assert {:error, %Ecto.Changeset{}} = WTTypeContext.update_wt_type(wt_type, @invalid_attrs)
#       assert wt_type == WTTypeContext.get_wt_type!(wt_type.id)
#     end

#     test "delete_wt_type/1 deletes the wt_type" do
#       wt_type = wt_type_fixture()
#       assert {:ok, %WTType{}} = WTTypeContext.delete_wt_type(wt_type)
#       assert_raise Ecto.NoResultsError, fn -> WTTypeContext.get_wt_type!(wt_type.id) end
#     end

#     test "change_wt_type/1 returns a wt_type changeset" do
#       wt_type = wt_type_fixture()
#       assert %Ecto.Changeset{} = WTTypeContext.change_wt_type(wt_type)
#     end
#   end
# end
