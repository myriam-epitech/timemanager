# defmodule TimemanagerWeb.ChartControllerTest do
#   use TimemanagerWeb.ConnCase

#   import Timemanager.ChartContextFixtures

#   alias Timemanager.ChartContext.Chart

#   @create_attrs %{
#     user_id: 42,
#     positionX: 120.5,
#     positionY: 120.5,
#     settings: %{},
#     type: "some type"
#   }
#   @update_attrs %{
#     user_id: 43,
#     positionX: 456.7,
#     positionY: 456.7,
#     settings: %{},
#     type: "some updated type"
#   }
#   @invalid_attrs %{user_id: nil, positionX: nil, positionY: nil, settings: nil, type: nil}

#   setup %{conn: conn} do
#     {:ok, conn: put_req_header(conn, "accept", "application/json")}
#   end

#   describe "index" do
#     test "lists all charts", %{conn: conn} do
#       conn = get(conn, Routes.chart_path(conn, :index))
#       assert json_response(conn, 200)["data"] == []
#     end
#   end

#   describe "create chart" do
#     test "renders chart when data is valid", %{conn: conn} do
#       conn = post(conn, Routes.chart_path(conn, :create), chart: @create_attrs)
#       assert %{"id" => id} = json_response(conn, 201)["data"]

#       conn = get(conn, Routes.chart_path(conn, :show, id))

#       assert %{
#                "id" => ^id,
#                "user_id" => 42,
#                "positionX" => 120.5,
#                "positionY" => 120.5,
#                "settings" => %{},
#                "type" => "some type"
#              } = json_response(conn, 200)["data"]
#     end

#     test "renders errors when data is invalid", %{conn: conn} do
#       conn = post(conn, Routes.chart_path(conn, :create), chart: @invalid_attrs)
#       assert json_response(conn, 422)["errors"] != %{}
#     end
#   end

#   describe "update chart" do
#     setup [:create_chart]

#     test "renders chart when data is valid", %{conn: conn, chart: %Chart{id: id} = chart} do
#       conn = put(conn, Routes.chart_path(conn, :update, chart), chart: @update_attrs)
#       assert %{"id" => ^id} = json_response(conn, 200)["data"]

#       conn = get(conn, Routes.chart_path(conn, :show, id))

#       assert %{
#                "id" => ^id,
#                "user_id" => 43,
#                "positionX" => 456.7,
#                "positionY" => 456.7,
#                "settings" => %{},
#                "type" => "some updated type"
#              } = json_response(conn, 200)["data"]
#     end

#     test "renders errors when data is invalid", %{conn: conn, chart: chart} do
#       conn = put(conn, Routes.chart_path(conn, :update, chart), chart: @invalid_attrs)
#       assert json_response(conn, 422)["errors"] != %{}
#     end
#   end

#   describe "delete chart" do
#     setup [:create_chart]

#     test "deletes chosen chart", %{conn: conn, chart: chart} do
#       conn = delete(conn, Routes.chart_path(conn, :delete, chart))
#       assert response(conn, 204)

#       assert_error_sent 404, fn ->
#         get(conn, Routes.chart_path(conn, :show, chart))
#       end
#     end
#   end

#   defp create_chart(_) do
#     chart = chart_fixture()
#     %{chart: chart}
#   end
# end
