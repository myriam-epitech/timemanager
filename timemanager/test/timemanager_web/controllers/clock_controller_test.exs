defmodule TimemanagerWeb.ClockControllerTest do
  use TimemanagerWeb.ConnCase
  import Timemanager.UserContextFixtures
  alias Timemanager.UserContext.User

  @create_attrs %{
    status: true,
    time: ~N[2022-10-24 07:20:00]
  }
  @update_attrs %{
    status: false,
    time: ~N[2022-10-25 07:20:00]
  }
  @invalid_attrs %{status: nil, time: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "getByUserID" do
    setup [:create_user]

    test "get the clock of an user", %{conn: conn, user: %User{} = user} do
      conn = get(conn, Routes.clock_path(conn, :getByUserID, user.id))
      assert json_response(conn, 404)["errors"] == nil
    end
  end

  describe "create clock" do
    setup [:create_user]

    test "renders clock when data is valid", %{conn: conn, user: %User{} = user} do
      conn = post(conn, Routes.clock_path(conn, :create, user.id), clock: @create_attrs)
      assert %{"id" => id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.clock_path(conn, :getByUserID, user.id))

      assert %{
               "id" => ^id,
               "status" => true,
               "time" => "2022-10-24T07:20:00"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, user: %User{} = user} do
      conn = post(conn, Routes.clock_path(conn, :create, user.id), clock: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update clock" do
    setup [:create_user]

    test "renders clock when data is valid", %{conn: conn, user: %User{} = user} do
      conn = post(conn, Routes.clock_path(conn, :create, user.id), clock: @create_attrs)
      assert %{"id" => id} = json_response(conn, 200)["data"]
      conn = get(conn, Routes.clock_path(conn, :getByUserID, user.id))

      assert %{
               "id" => ^id,
               "status" => true,
               "time" => "2022-10-24T07:20:00"
             } = json_response(conn, 200)["data"]

      conn = post(conn, Routes.clock_path(conn, :create, user.id), clock: @update_attrs)
      assert %{"id" => id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.clock_path(conn, :getByUserID, user.id))

      assert %{
               "id" => ^id,
               "status" => false,
               "time" => "2022-10-25T07:20:00"
             } = json_response(conn, 200)["data"]
    end
  end

  defp create_user(_) do
    user = user_fixture()
    %{user: user}
  end
end
