# defmodule TimemanagerWeb.WTTeamControllerTest do
#   use TimemanagerWeb.ConnCase

#   import Timemanager.WTTeamContextFixtures

#   alias Timemanager.WTTeamContext.WTTeam

#   @create_attrs %{
#     hour_worked: 120.5,
#     team_id: 42,
#     wt_id: 42
#   }
#   @update_attrs %{
#     hour_worked: 456.7,
#     team_id: 43,
#     wt_id: 43
#   }
#   @invalid_attrs %{hour_worked: nil, team_id: nil, wt_id: nil}

#   setup %{conn: conn} do
#     {:ok, conn: put_req_header(conn, "accept", "application/json")}
#   end

#   describe "index" do
#     test "lists all wt_teams", %{conn: conn} do
#       conn = get(conn, Routes.wt_team_path(conn, :index))
#       assert json_response(conn, 200)["data"] == []
#     end
#   end

#   describe "create wt_team" do
#     test "renders wt_team when data is valid", %{conn: conn} do
#       conn = post(conn, Routes.wt_team_path(conn, :create), wt_team: @create_attrs)
#       assert %{"id" => id} = json_response(conn, 201)["data"]

#       conn = get(conn, Routes.wt_team_path(conn, :show, id))

#       assert %{
#                "id" => ^id,
#                "hour_worked" => 120.5,
#                "team_id" => 42,
#                "wt_id" => 42
#              } = json_response(conn, 200)["data"]
#     end

#     test "renders errors when data is invalid", %{conn: conn} do
#       conn = post(conn, Routes.wt_team_path(conn, :create), wt_team: @invalid_attrs)
#       assert json_response(conn, 422)["errors"] != %{}
#     end
#   end

#   describe "update wt_team" do
#     setup [:create_wt_team]

#     test "renders wt_team when data is valid", %{conn: conn, wt_team: %WTTeam{id: id} = wt_team} do
#       conn = put(conn, Routes.wt_team_path(conn, :update, wt_team), wt_team: @update_attrs)
#       assert %{"id" => ^id} = json_response(conn, 200)["data"]

#       conn = get(conn, Routes.wt_team_path(conn, :show, id))

#       assert %{
#                "id" => ^id,
#                "hour_worked" => 456.7,
#                "team_id" => 43,
#                "wt_id" => 43
#              } = json_response(conn, 200)["data"]
#     end

#     test "renders errors when data is invalid", %{conn: conn, wt_team: wt_team} do
#       conn = put(conn, Routes.wt_team_path(conn, :update, wt_team), wt_team: @invalid_attrs)
#       assert json_response(conn, 422)["errors"] != %{}
#     end
#   end

#   describe "delete wt_team" do
#     setup [:create_wt_team]

#     test "deletes chosen wt_team", %{conn: conn, wt_team: wt_team} do
#       conn = delete(conn, Routes.wt_team_path(conn, :delete, wt_team))
#       assert response(conn, 204)

#       assert_error_sent 404, fn ->
#         get(conn, Routes.wt_team_path(conn, :show, wt_team))
#       end
#     end
#   end

#   defp create_wt_team(_) do
#     wt_team = wt_team_fixture()
#     %{wt_team: wt_team}
#   end
# end
