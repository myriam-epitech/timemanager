defmodule TimemanagerWeb.UserControllerTest do
  use TimemanagerWeb.ConnCase

  import Timemanager.UserContextFixtures
  # import Timemanager.RoleContextFixtures

  alias Timemanager.UserContext.User

  @create_attrs %{
    email: "some@email.fr",
    username: "someusername",
    password: "password",
    password_confirmation: "password",
    hourly_wage: 15
  }
  @update_attrs %{
    email: "updated@email.fr",
    username: "updatedusername",
    password: "updatedpassword",
    password_confirmation: "updatedpassword",
    hourly_wage: 10
  }
  @invalid_attrs %{email: nil, username: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all users", %{conn: conn} do
      conn = get(conn, Routes.user_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "index with email and username" do
    setup [:create_user]
    test "render the user who match email and password", %{conn: conn} do
      conn = get(conn, Routes.user_path(conn, :index), email: "fixture@email.fr", username: "fixtureusername")
      assert %{
        "email" => "fixture@email.fr",
        "hourly_wage" => 15.0,
        "username" => "fixtureusername",
        "night_hours_month" => 0,
        "role" => %{"data" => %{"id" => 1, "name" => "employee"}}
      } = json_response(conn, 200)["data"]
    end
  end

  describe "index with email" do
    setup [:create_user]
    test "render the user who match email", %{conn: conn} do
      conn = get(conn, Routes.user_path(conn, :index), email: "fixture2@email.fr")
      assert %{
        "email" => "fixture2@email.fr",
        "hourly_wage" => 25.0,
        "username" => "fixtureusername2",
        "night_hours_month" => 0,
        "role" => %{"data" => %{"id" => 1, "name" => "employee"}}
      } = json_response(conn, 200)["data"]
    end
  end

  describe "index with username" do
    setup [:create_user]
    test "render the user who match username", %{conn: conn} do
      conn = get(conn, Routes.user_path(conn, :index), username: "fixtureusername2")
      assert %{
        "email" => "fixture2@email.fr",
        "hourly_wage" => 25.0,
        "username" => "fixtureusername2",
        "night_hours_month" => 0,
        "role" => %{"data" => %{"id" => 1, "name" => "employee"}}
      } = json_response(conn, 200)["data"]
    end
  end


  describe "show user" do
    setup [:create_user]
    test "render the user who match id", %{conn: conn, user: user} do
      conn = get(conn, Routes.user_path(conn, :show, user))
      assert %{
        "email" => "fixture@email.fr",
        "hourly_wage" => 15.0,
        "username" => "fixtureusername",
        "night_hours_month" => 0,
        "role" => %{"data" => %{"id" => 1, "name" => "employee"}}
      } = json_response(conn, 200)["data"]
    end
  end



  describe "create user" do
    test "renders user when data is valid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :create), user: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.user_path(conn, :show, id))

      assert %{
               "email" => "some@email.fr",
               "hourly_wage" => 15.0,
               "id" => ^id,
               "username" => "someusername",
               "night_hours_month" => 0,
               "role" => %{"data" => %{"id" => 1, "name" => "employee"}}
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :create), user: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update user" do
    setup [:create_user]

    test "renders user when data is valid", %{conn: conn, user: %User{id: id} = user} do
      conn = put(conn, Routes.user_path(conn, :update, user), user: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.user_path(conn, :show, id))

      assert %{
               "email" => "updated@email.fr",
               "hourly_wage" => 10.0,
               "id" => ^id,
               "username" => "updatedusername",
               "night_hours_month" => 0,
               "role" => %{"data" => %{"id" => 1, "name" => "employee"}}
             } = json_response(conn, 200)["data"]

    end

    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn = put(conn, Routes.user_path(conn, :update, user), user: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete user" do
    setup [:create_user]

    test "deletes chosen user", %{conn: conn, user: user} do
      conn = delete(conn, Routes.user_path(conn, :delete, user))
      assert response(conn, 204)

      conn = get(conn, Routes.user_path(conn, :show, user))
      assert json_response(conn, 404)["errors"] != %{}
    end
  end

  defp create_user(_) do
    user = user_fixture()
    %{user: user}
  end
end
