# defmodule TimemanagerWeb.WTTypeControllerTest do
#   use TimemanagerWeb.ConnCase

#   import Timemanager.WTTypeContextFixtures

#   alias Timemanager.WTTypeContext.WTType

#   @create_attrs %{
#     name: "some name"
#   }
#   @update_attrs %{
#     name: "some updated name"
#   }
#   @invalid_attrs %{name: nil}

#   setup %{conn: conn} do
#     {:ok, conn: put_req_header(conn, "accept", "application/json")}
#   end

#   describe "index" do
#     test "lists all wt_types", %{conn: conn} do
#       conn = get(conn, Routes.wt_type_path(conn, :index))
#       assert json_response(conn, 200)["data"] == []
#     end
#   end

#   describe "create wt_type" do
#     test "renders wt_type when data is valid", %{conn: conn} do
#       conn = post(conn, Routes.wt_type_path(conn, :create), wt_type: @create_attrs)
#       assert %{"id" => id} = json_response(conn, 201)["data"]

#       conn = get(conn, Routes.wt_type_path(conn, :show, id))

#       assert %{
#                "id" => ^id,
#                "name" => "some name"
#              } = json_response(conn, 200)["data"]
#     end

#     test "renders errors when data is invalid", %{conn: conn} do
#       conn = post(conn, Routes.wt_type_path(conn, :create), wt_type: @invalid_attrs)
#       assert json_response(conn, 422)["errors"] != %{}
#     end
#   end

#   describe "update wt_type" do
#     setup [:create_wt_type]

#     test "renders wt_type when data is valid", %{conn: conn, wt_type: %WTType{id: id} = wt_type} do
#       conn = put(conn, Routes.wt_type_path(conn, :update, wt_type), wt_type: @update_attrs)
#       assert %{"id" => ^id} = json_response(conn, 200)["data"]

#       conn = get(conn, Routes.wt_type_path(conn, :show, id))

#       assert %{
#                "id" => ^id,
#                "name" => "some updated name"
#              } = json_response(conn, 200)["data"]
#     end

#     test "renders errors when data is invalid", %{conn: conn, wt_type: wt_type} do
#       conn = put(conn, Routes.wt_type_path(conn, :update, wt_type), wt_type: @invalid_attrs)
#       assert json_response(conn, 422)["errors"] != %{}
#     end
#   end

#   describe "delete wt_type" do
#     setup [:create_wt_type]

#     test "deletes chosen wt_type", %{conn: conn, wt_type: wt_type} do
#       conn = delete(conn, Routes.wt_type_path(conn, :delete, wt_type))
#       assert response(conn, 204)

#       assert_error_sent 404, fn ->
#         get(conn, Routes.wt_type_path(conn, :show, wt_type))
#       end
#     end
#   end

#   defp create_wt_type(_) do
#     wt_type = wt_type_fixture()
#     %{wt_type: wt_type}
#   end
# end
