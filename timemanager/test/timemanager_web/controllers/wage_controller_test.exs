# defmodule TimemanagerWeb.WageControllerTest do
#   use TimemanagerWeb.ConnCase

#   import Timemanager.WageContextFixtures

#   alias Timemanager.WageContext.Wage

#   @create_attrs %{
#     user_id: 42,
#     value: 120.5
#   }
#   @update_attrs %{
#     user_id: 43,
#     value: 456.7
#   }
#   @invalid_attrs %{user_id: nil, value: nil}

#   setup %{conn: conn} do
#     {:ok, conn: put_req_header(conn, "accept", "application/json")}
#   end

#   describe "index" do
#     test "lists all wages", %{conn: conn} do
#       conn = get(conn, Routes.wage_path(conn, :index))
#       assert json_response(conn, 200)["data"] == []
#     end
#   end

#   describe "create wage" do
#     test "renders wage when data is valid", %{conn: conn} do
#       conn = post(conn, Routes.wage_path(conn, :create), wage: @create_attrs)
#       assert %{"id" => id} = json_response(conn, 201)["data"]

#       conn = get(conn, Routes.wage_path(conn, :show, id))

#       assert %{
#                "id" => ^id,
#                "user_id" => 42,
#                "value" => 120.5
#              } = json_response(conn, 200)["data"]
#     end

#     test "renders errors when data is invalid", %{conn: conn} do
#       conn = post(conn, Routes.wage_path(conn, :create), wage: @invalid_attrs)
#       assert json_response(conn, 422)["errors"] != %{}
#     end
#   end

#   describe "update wage" do
#     setup [:create_wage]

#     test "renders wage when data is valid", %{conn: conn, wage: %Wage{id: id} = wage} do
#       conn = put(conn, Routes.wage_path(conn, :update, wage), wage: @update_attrs)
#       assert %{"id" => ^id} = json_response(conn, 200)["data"]

#       conn = get(conn, Routes.wage_path(conn, :show, id))

#       assert %{
#                "id" => ^id,
#                "user_id" => 43,
#                "value" => 456.7
#              } = json_response(conn, 200)["data"]
#     end

#     test "renders errors when data is invalid", %{conn: conn, wage: wage} do
#       conn = put(conn, Routes.wage_path(conn, :update, wage), wage: @invalid_attrs)
#       assert json_response(conn, 422)["errors"] != %{}
#     end
#   end

#   describe "delete wage" do
#     setup [:create_wage]

#     test "deletes chosen wage", %{conn: conn, wage: wage} do
#       conn = delete(conn, Routes.wage_path(conn, :delete, wage))
#       assert response(conn, 204)

#       assert_error_sent 404, fn ->
#         get(conn, Routes.wage_path(conn, :show, wage))
#       end
#     end
#   end

#   defp create_wage(_) do
#     wage = wage_fixture()
#     %{wage: wage}
#   end
# end
