defmodule TimemanagerWeb.WTControllerTest do
  use TimemanagerWeb.ConnCase

  import Timemanager.WTContextFixtures
  import Timemanager.UserContextFixtures

  alias Timemanager.UserContext.User
  alias Timemanager.WTContext.WT

  @create_attrs %{
    end: ~N[2022-10-24 06:20:00],
    start: ~N[2022-10-24 06:20:00],
    day_hours: 8,
    night_hours: 0,
    wt_type_id: 1
  }
  @update_attrs %{
    end: ~N[2022-10-25 07:21:00],
    start: ~N[2022-10-25 07:21:00],
    day_hours: 10,
    night_hours: 0,
    wt_type_id: 1
  }
  @invalid_attrs %{end: nil, start: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "getAll" do
    setup [:create_user]

    test "lists all workingtimes by User", %{conn: conn, user: %User{} = user} do
      conn = get(conn, Routes.wt_path(conn, :getAll, user.id))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create wt" do
    setup [:create_user]

    test "renders wt when data is valid", %{conn: conn, user: %User{} = user} do
      conn = post(conn, Routes.wt_path(conn, :create, user.id), wt: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.wt_path(conn, :getOne, user.id, id))

      assert %{
               "id" => ^id,
               "end" => "2022-10-24T06:20:00",
               "start" => "2022-10-24T06:20:00",
               "day_hours" => 8.0,
               "night_hours" => 0.0
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, user: %User{} = user} do
      conn = post(conn, Routes.wt_path(conn, :create, user.id), wt: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update wt" do
    setup [:create_wt]

    test "renders wt when data is valid", %{conn: conn, wt: %WT{id: id} = wt} do
      conn = put(conn, Routes.wt_path(conn, :update, id), wt: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.wt_path(conn, :getOne, wt.user_id, id))

      assert %{
               "id" => ^id,
               "end" => "2022-10-25T07:21:00",
               "start" => "2022-10-25T07:21:00"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, wt: %WT{id: id}} do
      conn = put(conn, Routes.wt_path(conn, :update, id), wt: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete wt" do
    setup [:create_wt]

    test "deletes chosen wt", %{conn: conn, wt: %WT{id: id} = wt} do
      conn = delete(conn, Routes.wt_path(conn, :delete, wt))
      assert response(conn, 204)

      conn = get(conn, Routes.wt_path(conn, :getOne,  wt.user_id, id))
      assert json_response(conn, 404)["errors"] != %{}
    end
  end

  defp create_wt(_) do
    wt = wt_fixture()
    %{wt: wt}
  end

  defp create_user(_) do
    user = user_fixture()
    %{user: user}
  end
end
