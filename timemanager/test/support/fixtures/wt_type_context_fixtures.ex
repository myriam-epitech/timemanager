defmodule Timemanager.WTTypeContextFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Timemanager.WTTypeContext` context.
  """

  @doc """
  Generate a wt_type.
  """
  def wt_type_fixture(attrs \\ %{}) do
    {:ok, wt_type} =
      attrs
      |> Enum.into(%{
        name: "workingfixture"
      })
      |> Timemanager.WTTypeContext.create_wt_type()

    wt_type
  end
end
