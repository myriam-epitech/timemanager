defmodule Timemanager.WageContextFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Timemanager.WageContext` context.
  """

  @doc """
  Generate a wage.
  """
  def wage_fixture(attrs \\ %{}) do
    {:ok, wage} =
      attrs
      |> Enum.into(%{
        user_id: 42,
        value: 120.5
      })
      |> Timemanager.WageContext.create_wage()

    wage
  end
end
