defmodule Timemanager.ChartContextFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Timemanager.ChartContext` context.
  """

  @doc """
  Generate a chart.
  """
  def chart_fixture(attrs \\ %{}) do
    {:ok, chart} =
      attrs
      |> Enum.into(%{
        id_user: 42,
        positionX: 120.5,
        positionY: 120.5,
        settings: %{},
        type: "some type"
      })
      |> Timemanager.ChartContext.create_chart()

    chart
  end
end
