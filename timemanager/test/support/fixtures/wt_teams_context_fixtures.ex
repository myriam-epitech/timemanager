defmodule Timemanager.WTTeamContextFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Timemanager.WTTeamContext` context.
  """

  @doc """
  Generate a wt_team.
  """
  def wt_team_fixture(attrs \\ %{}) do
    {:ok, wt_team} =
      attrs
      |> Enum.into(%{
        hour_worked: 120.5,
        team_id: 42,
        wt_id: 42
      })
      |> Timemanager.WTTeamContext.create_wt_team()

    wt_team
  end
end
