defmodule Timemanager.WTContextFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Timemanager.WTContext` context.
  """

  @doc """
  Generate a wt.
  """
  def wt_fixture(attrs \\ %{}) do
    wt_type = %{wt_type: Timemanager.WTTypeContextFixtures.wt_type_fixture()}
    user = %{user: Timemanager.UserContextFixtures.user_fixture()}

    wt_attrs =
      attrs
      |> Enum.into(%{
        end: ~N[2022-10-24 07:22:22],
        start: ~N[2022-10-24 07:22:22],
        day_hours: 8,
        night_hours: 0,
        wt_type_id: wt_type.wt_type.id,
        user_id: user.user.id
      })

        {:ok, wt} =Timemanager.WTContext.create_wt(wt_attrs, user.user.id)

    wt
  end
end
