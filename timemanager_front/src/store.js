import { createStore } from "vuex";

const store = createStore({
  state: {
    userConnected: [],
    charts: [],
    usersManaged: [],
  },
  getters: {
    authenticated(state) {
      return state.userConnected;
    },
  },
  mutations: {
    // USER CONNECTED
    setUserConnected(state, payload) {
      if (state.userConnected[0]) {
        state.userConnected[0] = payload;
      } else {
        state.userConnected.push(payload);
      }
    },
    updateUserConnected(state, payload) {
      state.userConnected[0] = payload;
    },
    clearUserConnected(state) {
      state.userConnected = [];
    },

    // USERS

    setUsersManaged(state, payload) {
      state.usersManaged.push(payload);
    },
    updateUsersManaged(state, payload) {
      var updatedUser = state.usersManaged.find(
        (user) => user.id == payload.id
      );
      updatedUser.params = payload.params;
    },
    removeUserManaged(state, payload) {
      var index = state.usersManaged.findIndex((user) => user.id == payload.id);
      state.usersManaged.splice(index, 1);
    },

    // CHARTS

    addChart(state, payload) {
      state.charts.push(payload);
    },
    deleteChart(state, payload) {
      var index = state.charts.findIndex((chart) => chart.id == payload);
      state.charts.splice(index, 1);
    },
    updateChartParams(state, payload) {
      var updatedChart = state.charts.find(
        (element) => element.id == payload.id
      );
      updatedChart.params = payload.params;
    },
    updateChartPosition(state, payload) {
      var updatedChart = state.charts.find(
        (element) => element.id == payload.id
      );
      updatedChart.positionX = payload.positionX;
      updatedChart.positionY = payload.positionY;
    },
  },
  actions: {
    getUser() {
      return this.state.userConnected;
    },
    init({ dispatch }) {
      return Promise.all([dispatch("getUser")]);
    },
  },
});

export default store;
