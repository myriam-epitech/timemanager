// export function getWorkinghours(workingtime, day = 7) {
//   let data = [];
//   let index = 0;
//   if (workingtime.length - day >= 0) {
//     index = workingtime.length - day;
//   }

//   for (let i = index; i < workingtime.length; i++) {
//     data.push(substractDate(workingtime[i].start, workingtime[i].end));
//   }
//   // workingtime.forEach((element) => {
//   //   data.push(substractDate(element.start, element.end));
//   // });
//   return data;
// }

export function getTotalWorkingHours(workingtime, startDay, endDay) {
  let startDate = new Date(startDay);
  let endDate = new Date(endDay);

  let data = [];
  let dateElement = "";
  let hoursWork = 0;

  workingtime.forEach((element, idx, array) => {
    if (isBetweenDate(new Date(element.start), startDate, endDate)) {
      if (dateElement == "") {
        dateElement = new Date(element.start);
      }

      if (array[idx + 1] === undefined) {
        if (!isDateEqual(dateElement, new Date(element.start))) {
          hoursWork = 0;
        }
        hoursWork += element.day_hours + element.night_hours;
        data.push(hoursWork);
        hoursWork = 0;
      }
      // Si dernier élément avant dépassement date fin
      else if (
        !isBetweenDate(new Date(array[idx + 1].start), startDate, endDate)
      ) {
        if (!isDateEqual(dateElement, new Date(element.start))) {
          hoursWork = 0;
        }
        hoursWork += element.day_hours + element.night_hours;
        data.push(hoursWork);
        hoursWork = 0;
      } else if (!isDateEqual(dateElement, new Date(element.start))) {
        data.push(hoursWork);
        hoursWork = 0;
        dateElement = new Date(element.start);
        hoursWork += element.day_hours + element.night_hours;
      } else if (isDateEqual(dateElement, new Date(element.start))) {
        hoursWork += element.day_hours + element.night_hours;
      }
    }
  });

  return data;
}

export function getDayWorkingHours(workingtime, startDay, endDay) {
  let startDate = new Date(startDay);
  let endDate = new Date(endDay);

  let data = [];
  let dateElement = "";
  let hoursWork = 0;

  workingtime.forEach((element, idx, array) => {
    if (isBetweenDate(new Date(element.start), startDate, endDate)) {
      if (dateElement == "") {
        dateElement = new Date(element.start);
      }

      if (array[idx + 1] === undefined) {
        if (!isDateEqual(dateElement, new Date(element.start))) {
          hoursWork = 0;
        }
        hoursWork += element.day_hours;
        data.push(hoursWork);
        hoursWork = 0;
      }
      // Si dernier élément avant dépassement date fin
      else if (
        !isBetweenDate(new Date(array[idx + 1].start), startDate, endDate)
      ) {
        if (!isDateEqual(dateElement, new Date(element.start))) {
          hoursWork = 0;
        }
        hoursWork += element.day_hours;
        data.push(hoursWork);
        hoursWork = 0;
      } else if (!isDateEqual(dateElement, new Date(element.start))) {
        data.push(hoursWork);
        hoursWork = 0;
        dateElement = new Date(element.start);
        hoursWork += element.day_hours;
      } else if (isDateEqual(dateElement, new Date(element.start))) {
        hoursWork += element.day_hours;
      }
    }
  });

  return data;
}

export function getNightWorkingHours(workingtime, startDay, endDay) {
  let startDate = new Date(startDay);
  let endDate = new Date(endDay);

  let data = [];
  let dateElement = "";
  let hoursWork = 0;

  workingtime.forEach((element, idx, array) => {
    if (isBetweenDate(new Date(element.start), startDate, endDate)) {
      if (dateElement == "") {
        dateElement = new Date(element.start);
      }
      if (array[idx + 1] === undefined) {
        if (!isDateEqual(dateElement, new Date(element.start))) {
          hoursWork = 0;
        }
        hoursWork += element.night_hours;
        data.push(hoursWork);
        hoursWork = 0;
      }
      // Si dernier élément avant dépassement date fin
      else if (
        !isBetweenDate(new Date(array[idx + 1].start), startDate, endDate)
      ) {
        if (!isDateEqual(dateElement, new Date(element.start))) {
          hoursWork = 0;
        }
        hoursWork += element.night_hours;
        data.push(hoursWork);
        hoursWork = 0;
      } else if (!isDateEqual(dateElement, new Date(element.start))) {
        data.push(hoursWork);
        hoursWork = 0;
        dateElement = new Date(element.start);
        hoursWork += element.night_hours;
      } else if (isDateEqual(dateElement, new Date(element.start))) {
        hoursWork += element.night_hours;
      }
    }
  });

  return data;
}

export function getAverageWorkingHours(workingtime, startDay, endDay) {
  let startDate = new Date(startDay);
  let endDate = new Date(endDay);

  let data = [];
  let dateElement = "";
  let hoursWork = 0;
  let month = "";
  let nbDay = 0;

  workingtime.forEach((element, idx, array) => {
    if (isBetweenDate(new Date(element.start), startDate, endDate)) {
      if (dateElement == "") {
        dateElement = new Date(element.start);
        month = dateElement.getMonth();
        nbDay++;
      }

      // Si prochain élément tableau n'existe pas , on est dans dernier element
      if (array[idx + 1] === undefined) {
        if (!isDateEqual(dateElement, new Date(element.start))) {
          if (new Date(element.start).getMonth() != month) {
            hoursWork = 0;
            nbDay = 0;
          }
        }
        nbDay++;
        hoursWork += element.day_hours + element.night_hours;
        data.push(parseFloat(hoursWork / nbDay).toFixed(2));
        hoursWork = 0;
        nbDay = 0;
      }
      // Si dernier élément avant dépassement date fin
      else if (
        !isBetweenDate(new Date(array[idx + 1].start), startDate, endDate)
      ) {
        if (!isDateEqual(dateElement, new Date(element.start))) {
          if (new Date(element.start).getMonth() != month) {
            hoursWork = 0;
            nbDay = 0;
          }
        }
        nbDay++;
        hoursWork += element.day_hours + element.night_hours;
        data.push(parseFloat(hoursWork / nbDay).toFixed(2));
        hoursWork = 0;
        nbDay = 0;
      }
      // Si jour différent
      else if (!isDateEqual(dateElement, new Date(element.start))) {
        if (new Date(element.start).getMonth() == month) {
          nbDay++;
          hoursWork += element.day_hours + element.night_hours;
        } else {
          data.push(parseFloat(hoursWork / nbDay).toFixed(2));
          nbDay = 0;
          hoursWork = 0;
          hoursWork += element.day_hours + element.night_hours;
        }
      }
      // Si même jour
      else if (isDateEqual(dateElement, new Date(element.start))) {
        hoursWork += element.day_hours + element.night_hours;
      }
      dateElement = new Date(element.start);
      month = dateElement.getMonth();
    }
  });

  return data;
}

export function getAverageWeekDayWorkingHours(workingtime, startDay, endDay) {
  const weekdays = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  var weekDay = "";
  let startDate = new Date(startDay);
  let endDate = new Date(endDay);

  let data = [];
  let dateElement = "";
  let hoursWork = 0;
  let nbDay = 0;
  let last = 0;

  for (let j = 1; j < weekdays.length; j++) {
    workingtime.forEach((element, idx, array) => {
      if (isBetweenDate(new Date(element.start), startDate, endDate)) {
        if (new Date(element.start).getDay() == j) {
          if (dateElement == "") {
            dateElement = new Date(element.start);
            if (weekDay === "") {
              weekDay = dateElement.getDay();
            }
            nbDay++;
          }

          // Si prochain élément tableau n'existe pas , on est dans dernier element
          if (array[idx + 1] === undefined) {
            last = 1;
            if (!isDateEqual(dateElement, new Date(element.start))) {
              nbDay++;
            }
            hoursWork += element.day_hours + element.night_hours;
            data.push(parseFloat(hoursWork / nbDay).toFixed(2));
            hoursWork = 0;
            nbDay = 0;
          }
          // Si dernier élément avant dépassement date fin
          else if (
            !isBetweenDate(new Date(array[idx + 1].start), startDate, endDate)
          ) {
            last = 1;
            if (!isDateEqual(dateElement, new Date(element.start))) {
              nbDay++;
            }
            hoursWork += element.day_hours + element.night_hours;
            data.push(parseFloat(hoursWork / nbDay).toFixed(2));
            hoursWork = 0;
            nbDay = 0;
          } else if (!isDateEqual(dateElement, new Date(element.start))) {
            nbDay++;
            hoursWork += element.day_hours + element.night_hours;
          } else if (isDateEqual(dateElement, new Date(element.start))) {
            hoursWork += element.day_hours + element.night_hours;
          }

          dateElement = new Date(element.start);
        }

        // Si prochain élément tableau n'existe pas , on est dans dernier element
        if (array[idx + 1] === undefined) {
          data.push(parseFloat(hoursWork / nbDay).toFixed(2));
          hoursWork = 0;
          nbDay = 0;
        } else if (
          !isBetweenDate(new Date(array[idx + 1].start), startDate, endDate) &&
          last == 0
        ) {
          data.push(parseFloat(hoursWork / nbDay).toFixed(2));
          hoursWork = 0;
          nbDay = 0;
        }
      }
    });
  }
  return data;
}

export function getLabelsMonth(workingtime, startDay, endDay) {
  let startDate = new Date(startDay);
  let endDate = new Date(endDay);

  let label = [];
  let dateElement = "";
  let month = "";
  const options = { month: "long" };

  workingtime.forEach((element, idx, array) => {
    if (isBetweenDate(new Date(element.start), startDate, endDate)) {
      if (dateElement == "") {
        dateElement = new Date(element.start);
        month = dateElement.getMonth();
        label.push(
          new Intl.DateTimeFormat("fr-FR", options).format(
            new Date(element.start)
          )
        );
      }

      // Si prochain élément tableau n'existe pas , on est dans dernier element
      if (array[idx + 1] === undefined) {
        if (!isDateEqual(dateElement, new Date(element.start))) {
          if (new Date(element.start).getMonth() != month) {
            label.push(
              new Intl.DateTimeFormat("fr-FR", options).format(
                new Date(element.start)
              )
            );
          }
        }
      }
      // Si dernier élément avant dépassement date fin
      else if (
        !isBetweenDate(new Date(array[idx + 1].start), startDate, endDate)
      ) {
        if (!isDateEqual(dateElement, new Date(element.start))) {
          if (new Date(element.start).getMonth() != month) {
            label.push(
              new Intl.DateTimeFormat("fr-FR", options).format(
                new Date(element.start)
              )
            );
          }
        }
      }
      // Si jour différent
      else if (!isDateEqual(dateElement, new Date(element.start))) {
        if (new Date(element.start).getMonth() == month) {
          console.log("");
        } else {
          label.push(
            new Intl.DateTimeFormat("fr-FR", options).format(
              new Date(element.start)
            )
          );
        }
      }
      // Si même jour
      else if (isDateEqual(dateElement, new Date(element.start))) {
        console.log("");
      }
      dateElement = new Date(element.start);
      month = dateElement.getMonth();
    }
  });

  return label;
}

export function getLabels(workingtime, startDay, endDay) {
  let label = [];

  var options = {
    month: "long",
    day: "numeric",
  };

  let startDate = new Date(startDay);
  let endDate = new Date(endDay);

  let dateElement = "";

  workingtime.forEach((element) => {
    if (isBetweenDate(new Date(element.start), startDate, endDate)) {
      if (dateElement == "") {
        dateElement = new Date(element.start);
        label.push(
          new Date(element.start).toLocaleDateString("fr-FR", options)
        );
      }
      // Si dernier élément du foreach
      if (!isDateEqual(dateElement, new Date(element.start))) {
        label.push(
          new Date(element.start).toLocaleDateString("fr-FR", options)
        );
      }
      dateElement = new Date(element.start);
    }
  });

  return label;
}

function isBetweenDate(date, start, end) {
  let ret = false;

  if (date >= start && date <= end) {
    ret = true;
  }

  return ret;
}

function isDateEqual(date1, date2) {
  let ret = false;
  if (
    date1.getFullYear() === date2.getFullYear() &&
    date1.getMonth() === date2.getMonth() &&
    date1.getDate() === date2.getDate()
  ) {
    ret = true;
  }

  return ret;
}
