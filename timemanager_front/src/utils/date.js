export function formatHour(_date) {
  const date = new Date(_date)
  const hour = date.getHours() + ':' + date.getMinutes()
  return hour
}

export function formatDate(_date) {
  const date = new Date(_date)
  return date.toLocaleDateString()
}


export function diffBetweenDatesInHour(a, b) {
  return Math.abs(new Date(a) - new Date(b)) / 3600000
}

export function countNightAndDayHours(start, end) {
  const start_hour = new Date(start).getHours()
  const end_hour = new Date(end).getHours()

  var night = 0
  var day = 0
  if (checkDateSameDay(start, end)) {
    if ((start_hour > 20 && end_hour > 20) || (start_hour <= 6 && end_hour <= 6)) {
      night = end_hour - start_hour
    } else if (start_hour > 6 && start_hour <= 20 && end_hour > 20) {
      night = end_hour - 21
      day = 21 - start_hour
    } else if (start_hour <= 6 && end_hour > 6 && end_hour < 21) {
      night = 6 - start_hour
      day = end_hour - 6
    } else if (start_hour <= 6 && end_hour > 20) {
      day = 21 - 6
      night = end_hour - 21 + 6 - start_hour
    } else {
      day = end_hour - start_hour
    }
  } else {
    if (start_hour > 20 && end_hour <= 6) {
      night = 24 - start_hour + end_hour
    } else if (start_hour > 20 && end_hour > 6) {
      day = end_hour - 6
      night = 24 - start_hour + 6
    } else if (start_hour <= 20 && end_hour > 6) {
      day = 21 - start_hour + end_hour - 6
      night = 24 - 21 + 6
    } else {
      day = 21 - start_hour
      night = 24 - 21 + end_hour
    }
  }
  return [night, day]
}

export function checkDateSameDay(start, end) {
  const first = new Date(start)
  const second = new Date(end)
  return first.getFullYear() === second.getFullYear() &&
    first.getMonth() === second.getMonth() &&
    first.getDate() === second.getDate();
}