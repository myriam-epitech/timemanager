function requests(_type, _url, _data, _header) {
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");
  localStorage.token &&
    myHeaders.append("authorization", "Bearer " + localStorage.token);
  if (_header) myHeaders.append(_header[0], _header[1]);

  var options = {
    method: _type,
    headers: myHeaders,
  };
  if (_data) options.body = JSON.stringify(_data);
  const request = new Request("http://3.86.238.144:4000/api/" + _url, options);
  return fetch(request);
}

// _____________________________________GET____________________________________________

//GET USER CONNECTED
export async function getUserConnected() {
  return await requests("GET", "currentUser", null);
}

//GET USERS
export async function getUsers(_params = "") {
  return await requests("GET", "users" + _params);
}

//GET USERS BY MANAGER
export async function getUsersByManager(_idManager) {
  return await requests("GET", "users/manager/" + _idManager);
}

//GET USER
export async function getUser(_id) {
  return await requests("GET", "users/" + _id);
}

//GET WORKING TIMES TYPE
export async function getWTtypes() {
  return await requests("GET", "wt/types");
}

//GET WORKING TIMES
export async function getWorkingTimes(_userId) {
  return await requests("GET", "workingtimes/" + _userId);
}

//GET WORKING TIMES
export async function getWorkingTimesOnPeriod(_userId, _start, _end) {
  return await requests(
    "GET",
    `workingtimes/${_userId}?start=${_start}&end=${_end}`
  );
}

//GET WORKING TIME
export async function getWorkingTime(_userId, _id) {
  return await requests("GET", "workingtimes/" + _userId + "/" + _id);
}

//GET USER'S FIRST WORKING TIME
export async function getFirstWorkingTime(_userId) {
  return await requests("GET", `workingtimes/${_userId}?which=first`);
}

//GET USER'S LAST WORKING TIME
export async function getLastWorkingTime(_userId) {
  return await requests("GET", `workingtimes/${_userId}?which=last`);
}

//GET CLOCK
export async function getClock(_userId) {
  return await requests("GET", "clocks/" + _userId);
}

//GET ALL TEAMS
export async function getAllTeams() {
  return await requests("GET", "teams");
}

//GET TEAMS BY MANAGER
export async function getTeamsByManager(_managerId) {
  return await requests("GET", "teams/manager/" + _managerId);
}

//GET WAGES BY USER
export async function getWagesByUser(_userId) {
  return await requests("GET", "wages/" + _userId);
}

//GET PROJECTS BY USER
export async function getProjectsByUser(_userId) {
  return await requests("GET", "projects/" + _userId);
}

//GET CHART
export async function getCharts() {
  return await requests("GET", "charts/");
}

// _____________________________________POST____________________________________________

//LOGIN
export async function loginUser(_body) {
  return await requests("POST", "login", _body);
}

//CREATE A NEW USER
export async function createUser(_data) {
  return await requests("POST", "users", _data);
}

//CREATE WORKING TIME
export async function createWorkingTime(_userId, _data) {
  return await requests("POST", "workingtimes/" + _userId, _data);
}

//CREATE CLOCK
export async function createOrRefreshClock(_userId, _data) {
  return await requests("POST", "clocks/" + _userId, _data);
}

//CREATE CLOCK
export async function createWage(_userId, _data) {
  return await requests("POST", "wages/" + _userId, _data);
}

// ADD USER TO TEAM
export async function addUserToTeam(_userId, _id) {
  return await requests("POST", "teams/" + _userId + "/" + _id);
}

//CREATE CHART
export async function createChart(_data) {
  return await requests("POST", "charts", _data);
}

//_____________________________________UPDATE____________________________________________
// ADD HOUR ON PROJECT
export async function declareHoursOnTeam(_userID, _idTeam, _idWT, _body) {
  return await requests(
    "POST",
    "users/" + _userID + "/teams/" + _idTeam + "/wt/" + _idWT,
    _body
  );
}

// CREATE TEAM
export async function createTeam(_body) {
  return await requests("POST", "teams", _body);
}

//_____________________________________UPDATE____________________________________________

//PATCH AN USER
export async function updateUser(id, _data) {
  return await requests("PUT", "users/" + id, _data);
}

//UPDATE WORKING TIME
export async function updateWT(_id, _data) {
  return await requests("PUT", "workingtimes/" + _id, _data);
}

//UPDATE TEAM
export async function updateTeam(_id, _data) {
  return await requests("PUT", "teams/" + _id, _data);
}

//UPDATE CHART
export async function updateChart(_id, _data) {
  return await requests("PUT", "charts/" + _id, _data);
}

// ADD MANAGER TO TEAM
export async function addManagerToTeam(_userId, _id, _body) {
  return await requests("PUT", "teams/" + _userId + "/" + _id, _body);
}
//_____________________________________DELETE____________________________________________
//DELETE AN USER
export async function deleteUser(id) {
  return await requests("DELETE", "users/" + id);
}
//DELETE WORKING TIME
export async function deleteWorkingTime(id) {
  return await requests("DELETE", "workingtimes/" + id);
}
//DELETE USER FROM TEAM
export async function deleteUserFromTeam(_id, _userId) {
  return await requests("DELETE", "teams/" + _userId + "/" + _id);
}
//DELETE A TEAM
export async function deleteTeam(_id) {
  return await requests("DELETE", "teams/" + _id);
}
//DELETE CHART
export async function deleteChart(id) {
  return await requests("DELETE", "charts/" + id);
}
