import store from "@/store"
import { createRouter, createWebHistory } from "vue-router";
import Profile from "../components/auth/Profile";
import WorkingTimes from "../components/WorkingTimes";
import WorkingTime from "../components/WorkingTime";
// import HelloWorld from "../components/HelloWorld";
import ClockManager from "../components/ClockManager";
import Dashboard from "../components/Dashboard";
import Manager from "../components/manager/Manager";
import ManageUsers from "../components/manager/ManageUsers";
import ManageTeams from "../components/manager/ManageTeams";
import LoginComponent from "../components/auth/Login";
import Projects from "../components/Projects";
import ChartManager from "../components/ChartManager";
import Unauthorized from "@/components/auth/Unauthorized"
import Wages from "@/components/Wages"
import { toRaw } from 'vue';

const getUserState = async () => {
  return new Promise((resolve) => {
    setTimeout(async () => {
      var user_proxy = await store.state.userConnected
      var user = toRaw(user_proxy)
      resolve(user)
    }, 2000)
  });
}

// const getUsersManagedState = async () => {
//   return new Promise((resolve, reject) => {
//     setTimeout(async () => {
//       var users_managed_proxy = await store.state.usersManaged
//       var users_managed = toRaw(users_managed_proxy)
//       resolve(users_managed)
//     }, 2000)
//   });
// }


function authGuard(role_id, user) {
  if (!user || user.length == 0 || (user[0] && user[0].role_id < role_id)) {
    return false;
  }
  return true;
}

const check_is_manager_of_user = (to) => {
  const users_managed_proxy = store.state.usersManaged
  const users_managed = JSON.parse(JSON.stringify(users_managed_proxy))
  const user_page_togo_id = to.params.userID
  const is_user_managed_by = users_managed.find(element => element.id == user_page_togo_id);
  if (is_user_managed_by == undefined) {
    return false
  }
  return true
}

const loggedGuard = (to, from, next) => {
  getUserState().then((res) => {
    const user = JSON.parse(JSON.stringify(res))
    const is_authorized = authGuard(1, user)
    if (!is_authorized) {
      next('/login')
    }
    next()
  });
}

const employeeGuard = (to, from, next) => {

  getUserState().then((res) => {
    const user = JSON.parse(JSON.stringify(res))
    const is_authorized = authGuard(1, user)
    const user_connected_id = user[0].id.toString()
    const user_page_togo_id = to.params.userID
    if (!is_authorized) {
      next('/unauthorized')
    }

    if (user_connected_id != user_page_togo_id) {
      const is_manager_of_user = check_is_manager_of_user(to)
      if (user[0].role_id == 2 && !is_manager_of_user) {
        next('/unauthorized')
      }
    }
    next()
  });
}

const managerGuard = (to, from, next) => {
  getUserState().then((res) => {
    const user = JSON.parse(JSON.stringify(res))

    const is_authorized = authGuard(2, user)

    if (!is_authorized) {
      next('/unauthorized')
    }
    next()
  });
}

const routes = [
  {
    path: "/",
    name: "home",
    component: Dashboard,
    beforeEnter: loggedGuard
  },
  {
    path: "/unauthorized",
    name: "unauthorized",
    component: Unauthorized,
  },
  {
    path: "/profile",
    component: Profile,
  },
  {
    path: "/login",
    name: "login",
    component: LoginComponent,
  },
  {
    path: "/dashboard",
    name: "dashboard",
    component: Dashboard,
  },
  {
    path: "/wages",
    name: "wages",
    component: Wages,
  },
  {
    path: "/manager",
    name: "manager",
    component: Manager,
    beforeEnter: managerGuard
  },
  {
    path: "/manager/users",
    name: "manageUsers",
    component: ManageUsers,
    beforeEnter: managerGuard
  },
  {
    path: "/manager/teams",
    name: "manageTeams",
    component: ManageTeams,
    beforeEnter: managerGuard
  },
  {
    path: "/projects/:userID",
    name: "projects",
    component: Projects,
    beforeEnter: employeeGuard
  },
  {
    path: "/workingTimes/:userID",
    name: "allWorkingTimes",
    component: WorkingTimes,
    beforeEnter: employeeGuard
  },
  {
    path: "/workingTime/:userID",
    name: "createWorkingTime",
    component: WorkingTime,
    beforeEnter: employeeGuard
  },
  {
    path: "/workingTime/:userID/:workingtimeID",
    name: "manageWorkingTime",
    component: WorkingTime,
    beforeEnter: employeeGuard
  },
  {
    path: "/clock/:userID",
    name: "clock",
    component: ClockManager,
    beforeEnter: employeeGuard
  },
  {
    path: "/chartManager/:userID",
    name: "manageCharts",
    component: ChartManager,
    beforeEnter: employeeGuard
  },
];

const router = createRouter({
  history: createWebHistory(),
  base: process.env.BASE_URL,
  routes, // short for `routes: routes`
});

export default router;